module.exports = {
    // mode: 'jit',
    important: true,
    content: ['./src/**/*.{html,ts}'],
    darkMode: 'media', // or 'media' or 'class'
    theme: {
        extend: {},
        colors: {
            primary: {
                default: 'var(--ion-color-primary)',
                shade: 'var(--ion-color-primary-shade)',
                tint: 'var(--ionic-color-primary-tint)',
            },
            secondary: {
                default: 'var(--ion-color-secondary)',
                shade: 'var(--ion-color-secondary-shade)',
                tint: 'var(--ionic-color-secondary-tint)',
            },
            tertiary: {
                default: 'var(--ion-color-tertiary)',
                shade: 'var(--ion-color-tertiary-shade)',
                tint: 'var(--ionic-color-tertiary-tint)',
            },
            success: {
                default: 'var(--ion-color-success)',
                shade: 'var(--ion-color-success-shade)',
                tint: 'var(--ionic-color-success-tint)',
            },
            warning: {
                default: 'var(--ion-color-warning)',
                shade: 'var(--ion-color-warning-shade)',
                tint: 'var(--ionic-color-warning-tint)',
            },
            danger: {
                default: 'var(--ion-color-danger)',
                shade: 'var(--ion-color-danger-shade)',
                tint: 'var(--ionic-color-danger-tint)',
            },
            dark: {
                default: 'var(--ion-color-dark)',
                shade: 'var(--ion-color-dark-shade)',
                tint: 'var(--ionic-color-dark-tint)',
            },
            medium: {
                default: 'var(--ion-color-medium)',
                shade: 'var(--ion-color-medium-shade)',
                tint: 'var(--ionic-color-medium-tint)',
            },
            light: {
                default: 'var(--ion-color-light)',
                shade: 'var(--ion-color-light-shade)',
                tint: 'var(--ionic-color-light-tint)',
            },
            tag0: 'var(--tag-0-color)',
            tag1: 'var(--tag-1-color)',
            tag2: 'var(--tag-2-color)',
            tag3: 'var(--tag-3-color)',
            tag4: 'var(--tag-4-color)',
            tag5: 'var(--tag-5-color)',
            tag6: 'var(--tag-6-color)',
            tag7: 'var(--tag-7-color)',
            tag8: 'var(--tag-8-color)',
            tag9: 'var(--tag-9-color)',
            tag10: 'var(--tag-10-color)',
            tag11: 'var(--tag-11-color)',
            tag12: 'var(--tag-12-color)',
            tag13: 'var(--tag-13-color)',
            tag14: 'var(--tag-14-color)',
            tag15: 'var(--tag-15-color)',
            tag16: 'var(--tag-16-color)',
            tag17: 'var(--tag-17-color)',
            tag18: 'var(--tag-18-color)',
        }
    },
    variants: {
        extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/forms'), require('@tailwindcss/line-clamp'), require('@tailwindcss/typography')],
};

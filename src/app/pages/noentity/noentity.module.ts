import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoentityPageRoutingModule } from './noentity-routing.module';

import { NoentityPage } from './noentity.page';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoentityPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [NoentityPage]
})
export class NoentityPageModule {}

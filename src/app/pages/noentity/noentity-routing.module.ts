import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoentityPage } from './noentity.page';

const routes: Routes = [
  {
    path: '',
    component: NoentityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoentityPageRoutingModule {}

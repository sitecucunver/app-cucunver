import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';
import { User } from '../../../interfaces/users';
import { NotesComponent } from '../../../components/notes/notes.component';

@Component({
    selector: 'app-note',
    templateUrl: './note.page.html',
    styleUrls: ['./note.page.scss'],
})
export class NotePage implements OnInit {

    user: User;
    @ViewChild(NotesComponent) notesComponent: NotesComponent;

    constructor(
        private helper: HelperService
    ) { }

    async ngOnInit() {
        this.user = await this.helper.getUser();
    }
}


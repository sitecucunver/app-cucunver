import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileService } from '../../../services/profile.service';
import { Profile, Workplace, LegalTutor, User } from '../../../interfaces/users';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { Country, Filter } from '../../../interfaces/generic';
import { IonContent, IonModal, ModalController, PopoverController } from '@ionic/angular';
import { WorkplacesComponent } from '../../../components/workplaces/workplaces.component';
import { SwiperOptions } from 'swiper';
import SwiperCore, {
    Pagination
} from 'swiper';
import { UiServiceService } from 'src/app/services/ui-service.service';
import { LegalTutorNewPage } from '../../modals/legal-tutor-new/legal-tutor-new.page';
import { HelperService } from '../../../services/helper.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

SwiperCore.use([Pagination]);
@Component({
    selector: 'app-profiledata',
    templateUrl: './profiledata.page.html',
    styleUrls: ['./profiledata.page.scss'],
})
export class ProfiledataPage implements OnInit {

    profile: Profile;
    user: User;
    legalTutors: LegalTutor[];
    countries: Country[];
    countriesWorkplace: Country[];
    workplace: Workplace = {
        id: 0,
        address: '',
        country: null,
        province: null,
        name: '',
        pc: '',
        town: '',
    };
    uploadAvatar: boolean = false;
    showWorkplaceData: boolean = false;
    searchWorkplaceInput: string = '';
    searchLegalTutorInput: string = '';
    height: number;
    fabNewClass: string = 'hidden';

    @ViewChild(IonContent) ionContent: IonContent;

    slideOptions: SwiperOptions = {
        pagination: true,
    };

    showErrorNif: boolean = false;
    activeProfile: boolean = true;
    activeLegalTutor: boolean = false;

    constructor(
        private profileService: ProfileService,
        private camera: Camera,
        private popoverControler: PopoverController,
        private uiService: UiServiceService,
        private modalController: ModalController,
        private helper: HelperService,
        private route: ActivatedRoute,
        private translate: TranslateService
    ) { }

    ngOnInit(): void {
        this.profileService.canceledPetition.subscribe(status => {
            if (status == 'OK') {
                this.setData();
            }
        });
        this.profileService.newLegalTutor.subscribe(status => {
            if (status) {
                this.setData();
            }
        });

        this.profileService.changedProfile.subscribe(async () => {
            this.setData();
        });
    }

    async ionViewDidEnter() {
        await this.setData();

        this.profileService.getCountries().then(resp => {
            this.countries = resp.result;
            this.countriesWorkplace = resp.result;
        });

        //Ejecutar acción de los parametros si llegan
        this.route.queryParams.subscribe(params => {
            if (params.profile_id) {
                const profileId = params.profile_id;
                if (this.user == null || this.user.currentProfile.id != profileId) {
                    this.profileService.changeProfile(profileId);
                } else {
                    this.setData();
                }
            } else {
                this.setData();
            }
        });
    }

    async setData(event?) {
        this.user = await this.helper.getUserAndSave();
        this.profile = this.user.currentProfile;

        if (this.user.showWorkplace) {
            if (this.profile.workplace) {
                this.workplace = this.profile.workplace;
            } else {
                this.workplace.country = this.profile.country;
                this.workplace.province = this.profile.province;
            }
        }

        if (this.profile.legalTutors && this.profile.legalTutors.length > 0) {
            this.legalTutors = this.profile.legalTutors;
        } else {
            this.legalTutors = this.profile.dependents;
        }

        if (event) {
            event.target.complete();
        }
    }

    takePhoto() {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.CAMERA
        };

        this.processingImage(options);
    }

    getGalleryPhoto() {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };

        this.processingImage(options);
    }

    processingImage(options: CameraOptions) {
        this.camera.getPicture(options).then((imageData) => {
            this.uploadAvatar = true;
            this.profile.avatar_tmp = 'data:image/jpeg;base64,' + imageData;
        });
    }

    compare = (o1, o2) => {
        return o1 === o2;
    };

    compareCountry = (o1, o2) => {
        return o1.id == o2.id;
    }

    searchWorkplace(event) {
        if (this.searchWorkplaceInput != '') {
            const filter: Filter[] = [
                {
                    field: 'name',
                    value: this.searchWorkplaceInput
                }
            ]
            this.profileService.getWorkplace(filter).then(obs => obs.subscribe(async resp => {
                const popover = await this.popoverControler.create({
                    component: WorkplacesComponent,
                    componentProps: { workplaces: resp.result },
                    event: event,
                    showBackdrop: false
                });
                await popover.present();

                let { data } = await popover.onWillDismiss();
                if (data.workplace) {
                    this.workplace = data.workplace;
                    this.searchWorkplaceInput = '';
                }
            }));
        }
    }

    saveProfile() {
        var form_data = this.helper.convertToFormData(this.profile, ['avatar', 'avatar_tmp'])

        if (this.uploadAvatar) {
            form_data.append('avatar', this.helper.base64ToImage(this.profile.avatar_tmp, 'image/jpeg'));
        }

        this.profileService.updateProfile(form_data).then(resp => {
            if (resp) {
                this.setData();
                this.ionContent.scrollToTop(500);
                this.uploadAvatar = false;
            }
        });
    }

    changeCountry(workplace?) {
        if (workplace) {
            if (this.workplace.country.working != 1) {
                this.workplace.province = '';
            } else {
                this.workplace.province = '';
            }
        } else {
            if (this.profile.country) {
                if (this.profile.country.working != 1) {
                    this.profile.province = '';
                } else {
                    this.profile.province = this.profile.province_tmp;
                }
            }
        }

    }

    moveToTop(event) {
        this.ionContent.scrollToTop(600);
        if (event.activeIndex == 1) {
            document.getElementById('legalTutor-btn-add').classList.remove('hidden');
            this.activeLegalTutor = true;
            this.activeProfile = false;
        } else {
            document.getElementById('legalTutor-btn-add').classList.add('hidden');
            this.activeLegalTutor = false;
            this.activeProfile = true;
        }
    }

    async addLegalTutor() {
        const modal = await this.modalController.create({
            component: LegalTutorNewPage,
            componentProps: {
                isYounger: this.profile.legalTutors && this.profile.legalTutors.length > 0,
                profile: this.profile,
                user: this.user
            },
            id: 'legal-tutor-new'
        });
        await modal.present();
    }

    changeDate(event, modal: IonModal) {
        this.profile.birthday = event.detail.value;
        modal.dismiss();
    }

    confirmSupervisor(id: number) {
        this.profileService.getProfile(id).then(resp => {
            if (resp.status == 'OK') {
                this.uiService.confirmUi(this.translate.instant('messages.aceptsupervisortitle'), this.translate.instant('messages.aceptsupervisormsg', { profile: resp.result.fullName }))
            }
        })
    }

    checkNIF() {
        if (this.profile.nif && this.profile.nif.length > 8) {
            const status = this.helper.checkNIF(this.profile.identification_type, this.profile.nif);

            if (status) {
                //Comprobar si existe
                const filter: Filter[] = [
                    {
                        field: 'nif',
                        value: this.profile.nif
                    },
                    {
                        field: 'entity_id',
                        value: this.profile.entity_id
                    }
                ];
                this.profileService.getProfiles(filter).then(resp => {
                    if (resp.status == 'OK') {

                        if (resp.result.length > 0 && resp.result.every(profile => profile.id != this.profile.id)) {
                            this.profile.nifError = 'document_registered';
                        } else {
                            this.profile.nifError = '';
                        }
                    } else {
                        this.profile.nifError = resp.error;
                    }
                }).catch(() => this.profile.nifError = 'genericerror');
            } else {
                this.profile.nifError = 'wrong-document';
            }
        }
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfiledataPage } from './profiledata.page';

const routes: Routes = [
  {
    path: '',
    component: ProfiledataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfiledataPageRoutingModule {}

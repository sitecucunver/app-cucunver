import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfiledataPageRoutingModule } from './profiledata-routing.module';

import { ProfiledataPage } from './profiledata.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../../pipes/pipes.module';
import { ComponentsModule } from '../../../components/components.module';

import { SwiperModule } from 'swiper/angular'
import { LegalTutorNewPageModule } from '../../modals/legal-tutor-new/legal-tutor-new.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfiledataPageRoutingModule,
    TranslateModule,
    PipesModule,
    ComponentsModule,
    SwiperModule,
    LegalTutorNewPageModule
  ],
  providers: [DatePipe],
  declarations: [ProfiledataPage]
})
export class ProfiledataPageModule {}

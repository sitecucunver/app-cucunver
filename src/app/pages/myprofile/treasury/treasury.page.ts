import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import SwiperCore, { Pagination, SwiperOptions } from 'swiper';
import { PaymethodActionsPage } from '../../modals/paymethod-actions/paymethod-actions.page';

SwiperCore.use([Pagination]);

@Component({
    selector: 'app-treasury',
    templateUrl: './treasury.page.html',
    styleUrls: ['./treasury.page.scss'],
})
export class TreasuryPage implements OnInit {

    @ViewChild(IonContent) ionContent: IonContent;
    slideOptions: SwiperOptions = {
        pagination: true,
    };
    activeQuota: boolean = true;
    activeBalance: boolean = false;
    activePaymethod: boolean = false;
    activeInvoice: boolean = false;

    constructor(
        private modalController: ModalController,
    ) { }

    ngOnInit() { }

    moveToTop(event) {
        this.ionContent.scrollToTop(600);
        this.activeQuota = false;
        this.activeBalance = false;
        this.activePaymethod = false;
        this.activeInvoice = false;
        switch (event.activeIndex) {
            case 0:
                this.activeQuota = true;
                break;
            case 1:
                this.activeBalance = true;
                break;
            case 2:
                this.activePaymethod = true;
                break;
            case 3:
                this.activeInvoice = true;
                break;
        }
        if (event.activeIndex == 2) {
            document.getElementById('paymethod-btn-add').classList.remove('hidden');
        } else {
            document.getElementById('paymethod-btn-add').classList.add('hidden');
        }
    }

    async addPaymethod() {
        const modal = await this.modalController.create({
            component: PaymethodActionsPage,
            componentProps: {
                action: 'new',
            },
            id: 'paymethod-action'
        });
        await modal.present();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TreasuryPageRoutingModule } from './treasury-routing.module';

import { TreasuryPage } from './treasury.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components/components.module';
import { SwiperModule } from 'swiper/angular';
import { PaymethodActionsPageModule } from '../../modals/paymethod-actions/paymethod-actions.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TreasuryPageRoutingModule,
    TranslateModule,
    ComponentsModule,
    SwiperModule,
    PaymethodActionsPageModule
  ],
  declarations: [TreasuryPage]
})
export class TreasuryPageModule {}

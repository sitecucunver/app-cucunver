import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPage } from './index.page';
import { TreasuryPage } from '../treasury/treasury.page';
import { AuthGuard } from '../../../guards/auth.guard';
import { DocumentsPageModule } from '../documents/documents.module';

const routes: Routes = [
    {
        path: '',
        component: IndexPage,
        children: [
            {
                path: 'profiledata',
                loadChildren: () => import('../profiledata/profiledata.module').then(m => m.ProfiledataPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'notes',
                loadChildren: () => import('../note/note.module').then(m => m.NotePageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'treasury',
                loadChildren: () => import('../treasury/treasury.module').then(m => m.TreasuryPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'documents',
                loadChildren: () => import('../documents/documents.module').then(m => m.DocumentsPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: '',
                redirectTo: '/myprofile/profiledata',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class IndexPageRoutingModule { }

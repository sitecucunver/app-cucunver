import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../../../services/helper.service';
import { User } from '../../../interfaces/users';
import { DocumentsComponent } from '../../../components/documents/documents.component';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.page.html',
    styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {

    user: User;
    @ViewChild(DocumentsComponent) documentsComponent: DocumentsComponent;

    constructor(
        private helper: HelperService
    ) { }

    async ngOnInit() {
        this.user = await this.helper.getUser();

    }

}

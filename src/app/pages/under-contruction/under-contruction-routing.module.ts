import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnderContructionPage } from './under-contruction.page';

const routes: Routes = [
  {
    path: '',
    component: UnderContructionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnderContructionPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnderContructionPageRoutingModule } from './under-contruction-routing.module';

import { UnderContructionPage } from './under-contruction.page';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnderContructionPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [UnderContructionPage]
})
export class UnderContructionPageModule {}

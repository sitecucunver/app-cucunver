import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, IonModal, ModalController } from '@ionic/angular';
import { HelperService } from '../../services/helper.service';
import { User } from '../../interfaces/users';
import { Timezone, Lang } from '../../interfaces/generic';
import { Observable } from 'rxjs';
import { ConfigurationService } from '../../services/configuration.service';
import { UiServiceService } from '../../services/ui-service.service';
import SwiperCore, {
    Pagination,
    SwiperOptions
} from 'swiper';
import { TranslateService } from '@ngx-translate/core';
import { DeleteAccountPage } from '../modals/delete-account/delete-account.page';

SwiperCore.use([Pagination]);

@Component({
    selector: 'app-configuration',
    templateUrl: './configuration.page.html',
    styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

    @ViewChild(IonContent) ionContent: IonContent;
    slideOptions: SwiperOptions = {
        pagination: true,
    };
    user: User;
    timezones: Observable<Timezone[]>;
    languages: Observable<Lang[]>;
    changePasswordObject = {
        currentPassword: '',
        newPassword: '',
        repeatNewPassword: ''
    };
    changeEmailObject = {
        newEmail: '',
        currentPassword: ''
    };
    isIndeterminate1: boolean;
    isIndeterminate2: boolean;
    masterCheck1: boolean;
    masterCheck2: boolean;

    constructor(
        private helper: HelperService,
        private configuration: ConfigurationService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private modalController: ModalController
    ) {
    }

    ngOnInit() {
        this.languages = this.helper.getLanguages();
        this.timezones = this.helper.getTimezones();
        this.helper.getUser().then(user => {
            this.user = user;
            this.masterCheck1 = this.user.options.activity_registry && this.user.options.email_notifications;
            this.masterCheck2 = this.user.options.me_activities && this.user.options.me_comunications_reception && this.user.options.me_data_request;
            if (!this.masterCheck1) {
                this.isIndeterminate1 = this.user.options.activity_registry || this.user.options.email_notifications;
            }

            if (!this.masterCheck2) {
                this.isIndeterminate2 = this.user.options.me_activities || this.user.options.me_comunications_reception || this.user.options.me_data_request;
            }
        });
    }

    changePassword(modal: IonModal) {
        if (this.changePasswordObject.currentPassword != '' && this.changePasswordObject.newPassword != '' && this.changePasswordObject.repeatNewPassword != '') {
            this.ui.presentLoading(this.translate.instant('messages.saving'));
            this.configuration.changePassword(this.changePasswordObject).then(resp => {
                this.ui.closeLoading();
                if (resp) {
                    this.ui.alertInfo(this.translate.instant('messages.updated-password'));
                    this.changePasswordObject.currentPassword = '';
                    this.changePasswordObject.newPassword = '';
                    this.changePasswordObject.repeatNewPassword = '';
                    modal.dismiss();
                }
            });
        }

    }

    changeEmail(modal: IonModal) {
        if (this.changeEmailObject.newEmail != '' && this.changeEmailObject.currentPassword != '') {
            this.ui.presentLoading(this.translate.instant('messages.saving'));
            this.configuration.changeEmail(this.changeEmailObject).then(resp => {
                this.ui.closeLoading();
                if (resp) {
                    this.ui.alertInfo(this.translate.instant('messages.message-mail-confirm'));
                    this.changeEmailObject.currentPassword = '';
                    this.changeEmailObject.newEmail = '';
                    modal.dismiss();
                }
            });
        }
    }

    async changeTimezoneAndLang() {
        await this.ui.presentLoading(this.translate.instant('messages.saving'));
        this.configuration.changeTimezoneAndLang(this.user).then(resp => {
            this.ui.closeLoading();
            if (resp) {
                this.ui.alertInfo(this.translate.instant('messages.language-and-region-changed'));
                this.translate.use(this.user.language);
            }
        });
    }

    async deleteAccount() {
        const modal = await this.modalController.create({
            component: DeleteAccountPage,
            id: 'delete-account'
        });
        return await modal.present();
    }

    checkMaster(group: string) {
        setTimeout(() => {
            if (group == 'notification-display') {
                this.user.options.activity_registry = this.masterCheck1;
                this.user.options.email_notifications = this.masterCheck1;
            } else if (group == 'me-notification') {
                this.user.options.me_comunications_reception = this.masterCheck2;
                this.user.options.me_data_request = this.masterCheck2;
                this.user.options.me_activities = this.masterCheck2;
            }
        });

    }

    checkEvent(group: string) {
        let checked = 0;
        if (group == 'notification-display') {
            if (!this.user.options.activity_registry) {
                this.masterCheck1 = false;
                checked++;
            }
            if (!this.user.options.email_notifications) {
                this.masterCheck1 = false;
                checked++;
            }
            if (checked > 0 && checked < 2) {
                this.isIndeterminate1 = true;
            } else if (checked >= 2) {
                this.isIndeterminate1 = false;
                this.masterCheck1 = false;

            } else {
                this.isIndeterminate1 = false;
                this.masterCheck1 = true;
            }

        } else if (group == 'me-notification') {
            if (!this.user.options.me_data_request) {
                this.masterCheck2 = false;
                checked++;
            }
            if (!this.user.options.me_activities) {
                this.masterCheck2 = false;
                checked++;
            }
            if (!this.user.options.me_comunications_reception) {
                this.masterCheck2 = false;
                checked++;
            }
            if (checked > 0 && checked < 3) {
                this.isIndeterminate2 = true;
            } else if (checked >= 3) {
                this.isIndeterminate2 = false;
                this.masterCheck2 = false;
            } else {
                this.isIndeterminate2 = false;
                this.masterCheck2 = true;
            }
        }
    }

    saveNotifications() {
        this.ui.presentLoading(this.translate.instant('messages.saving'));
        this.configuration.changeNotifications(this.user.options).then(resp => {
            this.ui.closeLoading();
            if (resp) {
                this.ui.alertInfo(this.translate.instant('messages.dataupdated'));
                this.translate.use(this.user.language);
            }
        });
    }
}

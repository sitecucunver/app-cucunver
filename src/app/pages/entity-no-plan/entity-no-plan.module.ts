import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntityNoPlanPageRoutingModule } from './entity-no-plan-routing.module';

import { EntityNoPlanPage } from './entity-no-plan.page';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntityNoPlanPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [EntityNoPlanPage]
})
export class EntityNoPlanPageModule {}

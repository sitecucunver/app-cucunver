import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntityNoPlanPage } from './entity-no-plan.page';

const routes: Routes = [
  {
    path: '',
    component: EntityNoPlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntityNoPlanPageRoutingModule {}

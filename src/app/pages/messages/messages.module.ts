import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessagesPageRoutingModule } from './messages-routing.module';

import { MessagesPage } from './messages.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';
import { MessageThreadPageModule } from '../modals/message-thread/message-thread.module';
import { MessageActionsPageModule } from '../modals/message-actions/message-actions.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessagesPageRoutingModule,
    ComponentsModule,
    TranslateModule,
    MessageThreadPageModule,
    MessageActionsPageModule,
  ],
  declarations: [MessagesPage]
})
export class MessagesPageModule {}

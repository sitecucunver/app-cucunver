import { Component, OnInit, ViewChild } from '@angular/core';
import { MessagesComponent } from '../../components/messages/messages.component';

@Component({
    selector: 'app-messages-page',
    templateUrl: './messages.page.html',
    styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {

    @ViewChild(MessagesComponent) messagesComponent: MessagesComponent;
    constructor() { }

    ngOnInit() {
    }
}

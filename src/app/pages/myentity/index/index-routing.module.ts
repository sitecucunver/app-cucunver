import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPage } from './index.page';
import { AuthGuard } from '../../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: IndexPage,
        children: [
            {
                path: 'entitydata',
                loadChildren: () => import('../entitydata/entitydata.module').then(m => m.EntitydataPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'documents',
                loadChildren: () => import('../documents/documents.module').then(m => m.DocumentsPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'affiliate-card',
                loadChildren: () => import('../affiliate-card/affiliate-card.module').then(m => m.AffiliateCardPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: '',
                redirectTo: '/myentity/entitydata',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/myentity/entitydata',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class IndexPageRoutingModule { }

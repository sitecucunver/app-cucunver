import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../services/helper.service';
import { User } from '../../../interfaces/users';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {
    user: User;

    constructor(
        private helper: HelperService
    ) { }

    ngOnInit() {
        this.helper.getUser().then(resp => {
            this.user = resp;
        });
    }

}

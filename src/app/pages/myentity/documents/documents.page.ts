import { Component, OnInit, ViewChild } from '@angular/core';
import { DocumentsComponent } from '../../../components/documents/documents.component';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.page.html',
    styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {

    @ViewChild(DocumentsComponent) documentsComponent: DocumentsComponent;
    constructor() { }

    ngOnInit() {

    }

}

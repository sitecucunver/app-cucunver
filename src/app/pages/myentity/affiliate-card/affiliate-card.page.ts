import { Component, OnDestroy, OnInit } from '@angular/core';
import { EntityService } from '../../../services/entity.service';
import { AffiliateCard, User } from '../../../interfaces/users';
import { UiServiceService } from '../../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { HelperService } from '../../../services/helper.service';
import { ScreenOrientation } from '@awesome-cordova-plugins/screen-orientation/ngx';

@Component({
    selector: 'app-affiliate-card',
    templateUrl: './affiliate-card.page.html',
    styleUrls: ['./affiliate-card.page.scss'],
})
export class AffiliateCardPage implements OnInit, OnDestroy {

    affiliateCard: AffiliateCard;
    user: User;
    qrCode: any;

    constructor(
        private entity: EntityService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private helper: HelperService,
        private screenOrientation: ScreenOrientation,
    ) { }

    ngOnInit() {
        this.getAffiliateCard();
        this.helper.getUser().then(resp => {
            this.user = resp;
        });

    }
    ionViewDidEnter() {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);

    }

    getAffiliateCard(event?) {
        this.entity.getAffiliateCard('affilaitetype').then(resp => {
            if (resp.status == 'OK') {
                this.affiliateCard = resp.result;
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')) );

    }

    ionViewWillLeave() {
        this.screenOrientation.unlock();
    }
    ngOnDestroy(): void {
        this.screenOrientation.unlock();
    }

}

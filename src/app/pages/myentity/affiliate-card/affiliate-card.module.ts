import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AffiliateCardPageRoutingModule } from './affiliate-card-routing.module';

import { AffiliateCardPage } from './affiliate-card.page';
import { TranslateModule } from '@ngx-translate/core';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AffiliateCardPageRoutingModule,
    TranslateModule,
    QRCodeModule
  ],
  declarations: [AffiliateCardPage]
})
export class AffiliateCardPageModule {}

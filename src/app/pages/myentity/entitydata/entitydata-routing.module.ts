import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntitydataPage } from './entitydata.page';

const routes: Routes = [
  {
    path: '',
    component: EntitydataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntitydataPageRoutingModule {}

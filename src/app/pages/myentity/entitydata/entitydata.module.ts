import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntitydataPageRoutingModule } from './entitydata-routing.module';

import { EntitydataPage } from './entitydata.page';
import { ComponentsModule } from '../../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntitydataPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [EntitydataPage]
})
export class EntitydataPageModule {}

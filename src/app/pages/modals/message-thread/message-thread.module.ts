import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessageThreadPageRoutingModule } from './message-thread-routing.module';

import { MessageThreadPage } from './message-thread.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../../pipes/pipes.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MessageThreadPageRoutingModule,
        TranslateModule,
        PipesModule,
        EditorModule,
        ComponentsModule
    ],
    declarations: [MessageThreadPage]
})
export class MessageThreadPageModule { }

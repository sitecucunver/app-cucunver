import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Message } from '../../../interfaces/messages';
import { MessagesService } from '../../../services/messages.service';
import { UiServiceService } from '../../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { MessageActionsPage } from '../message-actions/message-actions.page';

@Component({
    selector: 'app-message-thread',
    templateUrl: './message-thread.page.html',
    styleUrls: ['./message-thread.page.scss'],
})
export class MessageThreadPage implements OnInit {

    @Input() title: string;
    @Input() message: Message;
    messages: Message[];
    hasMore: boolean;
    page: number = 1;

    constructor(
        private modalController: ModalController,
        private messageService: MessagesService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private iab: InAppBrowser,
        private actionSheetController: ActionSheetController
    ) { }

    ngOnInit() {
        this.setData();
    }

    setData(event?) {
        this.getMessages(this.page);
        if (event) {
            event.target.complete();
        }
    }

    getMessages(page: number) {
        this.messageService.getMessageThread(this.message, 20, 'sender,receiver,editor', null, '', page).then(resp => {
            if (resp.status == 'OK') {
                this.messages = resp.result;
                this.hasMore = resp.lastPage > page;
                this.page = ++page
            } else {
                this.ui.alertInfo(resp.error);
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
    }

    close() {
        this.modalController.dismiss(false, '', 'message-thread');
    }

    async actions(message: Message) {
        if (message.iAmOwner) {
            const actionSheet = await this.actionSheetController.create({
                buttons: [
                    {
                        text: this.translate.instant('messages.edit'),
                        icon: 'pencil',
                        handler: () => {
                            this.edit(message);
                        },
                    },
                ],
            });
            await actionSheet.present();
        }
    }

    openFile(event: Event, path: string) {
        event.stopPropagation();
        this.iab.create(path, '_system', 'location=yes');
    }

    async edit(message: Message) {
        const modal = await this.modalController.create({
            component: MessageActionsPage,
            componentProps: {
                title: this.title,
                message,
                action: 'message-child-edit',
                canAttach: this.message.parent_id == null ? this.message.affectedAttach : this.message.parent.affectedAttach
            },
            id: 'message-actions'
        });
        await modal.present();

        const { data } = await modal.onDidDismiss();

        if (data && data.saved) {
            this.getMessages(1);
        }
    }

    async addMessage() {
        const modal = await this.modalController.create({
            component: MessageActionsPage,
            componentProps: {
                title: this.title,
                message: {parent_id: this.message.id, title: this.title},
                action: 'message-child-new',
                canAttach: this.message.affectedAttach
            },
            id: 'message-actions'
        });
        await modal.present();

        const { data } = await modal.onDidDismiss();

        if (data && data.saved) {
            this.getMessages(1);
        }
    }

}

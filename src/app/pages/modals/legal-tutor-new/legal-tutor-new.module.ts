import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LegalTutorNewPageRoutingModule } from './legal-tutor-new-routing.module';

import { LegalTutorNewPage } from './legal-tutor-new.page';
import { PipesModule } from '../../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LegalTutorNewPageRoutingModule,
        PipesModule,
        TranslateModule,
        ComponentsModule
    ],
    declarations: [LegalTutorNewPage],
})
export class LegalTutorNewPageModule { }

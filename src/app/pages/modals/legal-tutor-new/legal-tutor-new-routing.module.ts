import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LegalTutorNewPage } from './legal-tutor-new.page';

const routes: Routes = [
  {
    path: '',
    component: LegalTutorNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LegalTutorNewPageRoutingModule {}

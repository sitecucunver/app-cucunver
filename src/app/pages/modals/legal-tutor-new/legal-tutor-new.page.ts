import { Component, Input, ViewChild } from '@angular/core';
import { IonModal, ModalController } from '@ionic/angular';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { Country } from 'src/app/interfaces/generic';
import { ProfileService } from '../../../services/profile.service';
import { NgForm } from '@angular/forms';
import { HelperService } from '../../../services/helper.service';
import { Filter } from '../../../interfaces/generic';
import { TranslateService } from '@ngx-translate/core';
import { format, parseISO } from 'date-fns';
import { UiServiceService } from '../../../services/ui-service.service';
import { User, Profile } from '../../../interfaces/users';

@Component({
    selector: 'app-legal-tutor-new',
    templateUrl: './legal-tutor-new.page.html',
    styleUrls: ['./legal-tutor-new.page.scss'],
})
export class LegalTutorNewPage {

    @Input() isYounger: boolean;
    @Input() profile: Profile;
    @Input() user: User;
    @ViewChild('fLegalTutor') fLegalTutor: NgForm;
    @ViewChild('fLegalTutorNew') fLegalTutorNew: NgForm;

    step1Class: string;
    step2Class: string = 'hidden';
    newLegalTutor: any;
    uploadAvatar: boolean = false;
    countries: Country[];
    caducity: string;
    years: number[] = [];
    showErrorNif: boolean = false;

    constructor(
        private modalController: ModalController,
        private camera: Camera,
        private profileService: ProfileService,
        private helperService: HelperService,
        private translate: TranslateService,
        private uiService: UiServiceService
    ) { }

    ionViewDidEnter() {
        this.profileService.getCountries().then(resp => {
            this.countries = resp.result;
            this.setDefault();
        });
    }

    close() {
        this.modalController.dismiss('','', 'legal-tutor-new');
    }

    continue() {
        if (this.fLegalTutor.valid) {
            this.step1Class = 'hidden';
            this.step2Class = '';
        }
    }

    back() {
        this.step2Class = 'hidden';
        this.step1Class = '';
    }

    async save() {
        if (this.fLegalTutorNew.valid) {
            this.uiService.presentLoading(this.translate.instant('messages.saving'));
            var form_data = this.helperService.convertToFormData(this.newLegalTutor, ['avatar'])

            if (this.uploadAvatar) {
                form_data.append('avatar', this.helperService.base64ToImage(this.newLegalTutor.avatar, 'image/jpeg'));
            }
            const status = await this.profileService.legalTutorStore(form_data);
            if (status) {
                this.close();
                this.uiService.closeLoading();
            } else {
                this.uiService.closeLoading();
            }
        }
    }

    takePhoto() {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.CAMERA
        };

        this.processingImage(options);
    }

    getGalleryPhoto() {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };

        this.processingImage(options);
    }

    processingImage(options: CameraOptions) {
        this.camera.getPicture(options).then((imageData) => {
            this.uploadAvatar = true;
            this.newLegalTutor.avatar = 'data:image/jpeg;base64,' + imageData;
        });
    }

    changeCountry(event) {
        this.newLegalTutor.country = this.countries.find(country => country.id == event.detail.value);
        if (this.newLegalTutor.country.working != 1) {
            this.newLegalTutor.province = '';
        } else {
            this.newLegalTutor.province = this.newLegalTutor.province_tmp;
        }
    }

    setDefault() {
        this.newLegalTutor = {
            profileId: this.profile.id,
            relationship: '',
            name: '',
            lastName: '',
            identification_type: this.user.currentEntity.fiscalCountry == 183 ? 5 : 1,
            nif: '',
            nifError: '',
            email: '',
            birthday: '',
            gender: 0,
            address: '',
            province: this.profile.province,
            town: '',
            pc: '',
            phone2: '',
            phone: '',
            country: this.profile.country,
            paymethod: '0',
            holder: '',
            number: '',
            caducity: '',
            cvv: '',
            type: this.isYounger ? 'tutor' : 'younger',
            avatar: null
        };
        this.uploadAvatar = false;
        this.years = this.helperService.getYearsAgo(5);
    }

    checkNIF() {
        if (this.newLegalTutor.nif && this.newLegalTutor.nif.length > 8) {
            const status = this.helperService.checkNIF(this.newLegalTutor.identification_type, this.newLegalTutor.nif);

            if (status) {
                //Comprobar si existe
                const filter: Filter[] = [
                    {
                        field: 'nif',
                        value: this.newLegalTutor.nif
                    },
                    {
                        field: 'entity_id',
                        value: this.user.entity_id
                    }
                ];
                this.profileService.getProfiles(filter).then(resp => {
                    if (resp.status == 'OK') {

                        if (resp.result.length > 0) {
                            this.newLegalTutor.nifError = 'document_registered';
                        } else {
                            this.newLegalTutor.nifError = '';
                        }
                    } else {
                        this.newLegalTutor.nifError = resp.error;
                    }
                }).catch(() => this.newLegalTutor.nifError = 'genericerror');
            } else {
                this.newLegalTutor.nifError = 'wrong-document';
            }
        }
    }

    async changeDate(event: any, modal: IonModal, property: string) {
        if (property == 'caducity') {
            await event.confirm();
            const value = event.value;

            this.newLegalTutor[property] = format(parseISO(value), 'MM/yyyy');
            this.caducity = value;
        } else {
            this.newLegalTutor[property] = event.detail.value;
        }
        modal.dismiss();
    }

    changePaymethodType() {
        this.newLegalTutor.holder = '';
        this.newLegalTutor.number = '';
    }
}

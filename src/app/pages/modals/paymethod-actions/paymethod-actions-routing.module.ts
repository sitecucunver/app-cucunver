import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymethodActionsPage } from './paymethod-actions.page';

const routes: Routes = [
  {
    path: '',
    component: PaymethodActionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymethodActionsPageRoutingModule {}

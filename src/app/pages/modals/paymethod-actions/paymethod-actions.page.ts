import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController, IonModal } from '@ionic/angular';
import { format, parseISO } from 'date-fns';
import { Paymethod } from '../../../interfaces/treasury';
import { NgForm } from '@angular/forms';
import { HelperService } from '../../../services/helper.service';
import { User } from '../../../interfaces/users';
import { TreasuryService } from '../../../services/treasury.service';
import { UiServiceService } from '../../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-paymethod-actions',
    templateUrl: './paymethod-actions.page.html',
    styleUrls: ['./paymethod-actions.page.scss'],
})
export class PaymethodActionsPage implements OnInit {

    @Input() action: string;
    @Input() paymethod: Paymethod;
    @ViewChild('fPaymethodNew') fPaymethodNew: NgForm;
    caducity: string;
    years: number[] = [];
    user: User;
    existsType1: boolean;
    existsType2: boolean;
    showCreditCard: boolean;

    constructor(
        private modalController: ModalController,
        private helper: HelperService,
        private treasuryService: TreasuryService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    ngOnInit() {
        this.years = this.helper.getYearsAgo(5);
        if (this.paymethod == null) {
            this.setDefault();
        } else {
            if (this.paymethod.type == 4) {
                var date = this.paymethod.caducity.match(/([01]\d)\/(\d{4})/);
                this.caducity = new Date(parseInt(date[1]), parseInt(date[2])).toISOString();
            }
        }
    }

    async setDefault() {
        this.user = await this.helper.getUser();
        this.paymethod = {
            profile_id: this.user.profile_id,
        };
        const existentPayments = this.user.currentProfile.payments;
        this.showCreditCard = this.user.currentProfile.showCreditCard;
        this.existsType1 = existentPayments.find(paymethod => paymethod.type == 1) != null;
        this.existsType2 = existentPayments.find(paymethod => paymethod.type == 2) != null;

        if (!this.existsType1) {
            this.paymethod.type = 1;
        } else {
            if (this.existsType2) {
                this.paymethod.type = 3
            } else {
                this.paymethod.type = 2;
            }
        }
    }

    changePaymethodType() {
        this.paymethod.holder = '';
        this.paymethod.number = '';
    }

    close() {
        this.modalController.dismiss('', '', 'paymethod-action');
    }

    async save() {
        if (this.fPaymethodNew.valid) {
            await this.ui.presentLoading(this.translate.instant('messages.saving'));
            var status;
            if (this.paymethod.id == null) {
                status = await this.treasuryService.paymethodStore(this.paymethod);
            } else {
                status = await this.treasuryService.paymethodUpdate(this.paymethod);
            }
            if (status) {
                this.close();
            }

            this.ui.closeLoading();
        }
    }

    changeDate(event, modal: IonModal) {
        this.paymethod.caducity = format(parseISO(event.detail.value), 'MM/yyyy');
        this.caducity = event.detail.value;
        modal.dismiss();
    }

    reset(modal: IonModal) {
        this.caducity = null;
        this.paymethod.caducity = null;
        modal.dismiss();
    }
}

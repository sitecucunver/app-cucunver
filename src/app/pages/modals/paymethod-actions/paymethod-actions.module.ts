import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymethodActionsPageRoutingModule } from './paymethod-actions-routing.module';

import { PaymethodActionsPage } from './paymethod-actions.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymethodActionsPageRoutingModule,
    TranslateModule,
    ComponentsModule
  ],
  declarations: [PaymethodActionsPage]
})
export class PaymethodActionsPageModule {}

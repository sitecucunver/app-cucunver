import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessageActionsPageRoutingModule } from './message-actions-routing.module';

import { MessageActionsPage } from './message-actions.page';
import { EditorModule } from '@tinymce/tinymce-angular';
import { PipesModule } from '../../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MessageActionsPageRoutingModule,
        PipesModule,
        TranslateModule,
        ComponentsModule,
        EditorModule
    ],
    declarations: [MessageActionsPage]
})
export class MessageActionsPageModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessageActionsPage } from './message-actions.page';

const routes: Routes = [
  {
    path: '',
    component: MessageActionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MessageActionsPageRoutingModule {}

import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Message } from '../../../interfaces/messages';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../../services/ui-service.service';
import { MessagesService } from '../../../services/messages.service';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { HelperService } from '../../../services/helper.service';

@Component({
    selector: 'app-message-actions',
    templateUrl: './message-actions.page.html',
    styleUrls: ['./message-actions.page.scss'],
})
export class MessageActionsPage {

    @Input() title: string;
    @Input() message: Message;
    @Input() action: string;
    @Input() canAttach: boolean;
    tinyMceInit;
    tinyMceApiKey: string;

    constructor(
        private modalController: ModalController,
        private translate: TranslateService,
        private ui: UiServiceService,
        private messageService: MessagesService,
        private chooser: Chooser,
        private helper: HelperService
    ) { }

    ionViewWillEnter() {
        const config = this.helper.getTyinymceConfig();
        this.tinyMceInit = config.options;
        this.tinyMceApiKey = config.apiKey;
    }

    close(data?) {
        this.modalController.dismiss(data, '', 'message-actions');
    }

    attach() {
        this.chooser.getFile()
            .then(file => {
                if (file) {
                    this.message.fileName = null;
                    this.message.filePath = null;
                    //Eliminar el fichero del servidor
                    this.message.file = file;
                    this.message.fileFlag = 'new';
                }
            })
            .catch((error: any) => console.error(error));
    }

    async send() {
        await this.ui.presentLoading(this.translate.instant('messages.saving'));
        const form_data = this.helper.convertToFormData(this.message, ['file']);
            if (this.message.file) {
                form_data.append('file', this.helper.base64ToImage(this.message.file.dataURI, this.message.file.mediaType), this.message.file.name);
            }
        if (this.message.id == null) {
            if (this.message.parent_id == null) {
                if (this.message.title == null) {
                    this.ui.closeLoading();
                    this.ui.alertInfo(this.translate.instant('errors.empty-fields'));
                } else {
                    this.messageService.store(form_data).then(result => {
                        this.ui.closeLoading();

                        if (result) {
                            this.close({saved: true});
                        }
                    }).catch(() => this.ui.closeLoading());
                }
            } else {
                this.messageService.storeChild(form_data).then(result => {
                    this.ui.closeLoading();
                    if (result) {
                        this.close({saved: true});
                    }
                }).catch(() => this.ui.closeLoading());
            }
        } else {
            this.messageService.update(form_data).then(result => {
                this.ui.closeLoading();
                if (result) {
                    this.close({saved: true});
                }
            }).catch(() => this.ui.closeLoading());
        }
    }

    deleteFile() {
        if (this.message.id) {
            this.messageService.deleteFile(this.message.id).then(resp => {
                if (resp) {
                    this.message.fileName = null;
                    this.message.filePath = null;
                    this.message.file = null;
                    this.message.fileFlag = null;
                }
            });
        } else {
            this.message.fileName = null;
            this.message.filePath = null;
            this.message.file = null;
            this.message.fileFlag = null;
        }

    }
}

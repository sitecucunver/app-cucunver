import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { NoteService } from '../../../services/note.service';
import { UiServiceService } from '../../../services/ui-service.service';
import { Note } from '../../../interfaces/generic';
import { HelperService } from '../../../services/helper.service';

@Component({
    selector: 'app-note-new',
    templateUrl: './note-action.page.html',
    styleUrls: ['./note-action.page.scss'],
})
export class NoteActionPage implements OnInit {

    @Input() note: Note;
    tinyMceInit;
    tinyMceApiKey: string;
    @Input() title: string;

    constructor(
        private modalController: ModalController,
        private translate: TranslateService,
        private chooser: Chooser,
        private noteService: NoteService,
        private uiService: UiServiceService,
        private helper: HelperService
    ) { }

    ngOnInit() {
        const config = this.helper.getTyinymceConfig();
        this.tinyMceInit = config.options;
        this.tinyMceApiKey = config.apiKey;
    }

    close(data?) {
        this.modalController.dismiss(data);
    }

    attach() {
        this.chooser.getFile()
            .then(file => {
                if (file) {
                    this.note.fileName = null;
                    this.note.filePath = null;
                    //Eliminar el fichero del servidor
                    this.note.file = file;
                    this.note.fileFlag = 'new';
                }
            })
            .catch((error: any) => console.error(error));
    }

    send() {
        this.uiService.presentLoading(this.translate.instant('messages.saving'));
        const form_data = this.helper.convertToFormData(this.note, ['file']);

        if (this.note.file) {
            form_data.append('file', this.helper.base64ToImage(this.note.file.dataURI, this.note.file.mediaType), this.note.file.name);
        }

        if (this.note.id == null) {
            this.noteService.storeNote(form_data).then(result => {
                this.uiService.closeLoading();
                if (result) {
                    this.close({ saved: true });
                }
            });
        } else {
            this.noteService.updateNote(form_data).then(result => {
                this.uiService.closeLoading();
                if (result) {
                    this.close({ saved: true });
                }
            });
        }
    }

    deleteFile() {
        if (this.note.id) {
            this.noteService.deleteFile(this.note.id).then(resp => {
                if (resp) {
                    this.note.fileName = null;
                    this.note.filePath = null;
                    this.note.file = null;
                    this.note.fileFlag = null;
                }
            });
        } else {
            this.note.fileName = null;
            this.note.filePath = null;
            this.note.file = null;
            this.note.fileFlag = null;
        }

    }
}

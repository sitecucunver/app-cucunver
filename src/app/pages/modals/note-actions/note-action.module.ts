import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoteActionPageRoutingModule } from './note-action-routing.module';

import { NoteActionPage } from './note-action.page';
import { PipesModule } from '../../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components/components.module';
import { EditorModule } from '@tinymce/tinymce-angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NoteActionPageRoutingModule,
        PipesModule,
        TranslateModule,
        ComponentsModule,
        EditorModule
    ],
    declarations: [NoteActionPage],
})
export class NoteActionPageModule { }

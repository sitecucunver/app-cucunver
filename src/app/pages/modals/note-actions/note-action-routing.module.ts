import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoteActionPage } from './note-action.page';

const routes: Routes = [
  {
    path: '',
    component: NoteActionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoteActionPageRoutingModule {}

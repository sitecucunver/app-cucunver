import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from '../../../services/configuration.service';
import { UiServiceService } from '../../../services/ui-service.service';
import { LoginService } from '../../../services/login.service';

@Component({
    selector: 'app-delete-account',
    templateUrl: './delete-account.page.html',
    styleUrls: ['./delete-account.page.scss'],
})
export class DeleteAccountPage implements OnInit {

    deleteAccountObject = {
        currentPassword: '',
        reason: null,
        text: ''
    };

    constructor(
        private modalController: ModalController,
        private configuration: ConfigurationService,
        private navController: NavController,
        private ui: UiServiceService,
        private translate: TranslateService,
        private login: LoginService
    ) { }

    ngOnInit() {
    }

    close() {
        this.modalController.dismiss('', '', 'delete-account');
    }

    deleteAccount() {
        this.ui.presentLoading(this.translate.instant('messages.deleting'));
        if (this.deleteAccountObject.currentPassword != '' && (this.deleteAccountObject.reason != null || (this.deleteAccountObject.reason == 5 && this.deleteAccountObject.text != ''))) {
            this.configuration.deleteAccount(this.deleteAccountObject).then(resp => {
                this.ui.closeLoading();
                if (resp) {
                    this.close();
                    this.login.logout('/deleted-account');
                }
            });
        }
    }

}

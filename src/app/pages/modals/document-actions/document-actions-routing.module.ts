import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentActionsPage } from './document-actions.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentActionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentActionsPageRoutingModule {}

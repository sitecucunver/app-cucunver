import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentActionsPageRoutingModule } from './document-actions-routing.module';

import { DocumentActionsPage } from './document-actions.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentActionsPageRoutingModule,
    TranslateModule,
    PipesModule
  ],
  declarations: [DocumentActionsPage]
})
export class DocumentActionsPageModule {}

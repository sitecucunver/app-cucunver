import { Component, OnInit, Input } from '@angular/core';
import { CucunverFile } from '../../../interfaces/files';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../../services/ui-service.service';
import { DocumentsService } from '../../../services/documents.service';

@Component({
    selector: 'app-document-actions',
    templateUrl: './document-actions.page.html',
    styleUrls: ['./document-actions.page.scss'],
})
export class DocumentActionsPage implements OnInit {

    @Input() cucunverFile: CucunverFile;
    @Input() hasActions: boolean;
    imgs: any[] = [];

    constructor(
        private modalController: ModalController,
        private translate: TranslateService,
        private ui: UiServiceService,
        private documentService: DocumentsService
    ) { }

    ngOnInit() {}

    close(data?) {
        this.modalController.dismiss(data);
    }

    async addFile() {
        if (this.hasActions) {
            await this.ui.presentLoading(this.translate.instant('messages.saving'));
            let formData = new FormData();
            formData.append('type', this.cucunverFile.type.toString());
            for (let i = 0; i < this.cucunverFile.file.length; i++) {
                formData.append('file[]', this.cucunverFile.file[i], this.cucunverFile.file[i].name);
            }
            formData.append('section', this.cucunverFile.section);
            formData.append('affected_id', this.cucunverFile.affected_id.toString());
            formData.append('parent_id', this.cucunverFile.parent_id.toString());
            const status = await this.documentService.storeFile(formData);
            this.ui.closeLoading();
            if (status) {
                this.close({ saved: true });
            } else {
                this.ui.closeLoading();
            }
        }
    }

    onFileChange(ev) {
        this.cucunverFile.file = ev.target.files;
    }

    async createFolder() {
        this.ui.presentLoading(this.translate.instant('messages.saving'));
        var status, type;
        if (this.cucunverFile.id) {
            type = 'update';
            status = await this.documentService.updateFile(this.cucunverFile);
        } else {
            type = 'store';
            status = await this.documentService.storeFile(this.cucunverFile);
        }

        this.ui.closeLoading();
        if (status) {
            this.close({ saved: true })
        }
    }

    save() {
        if (this.cucunverFile.type == 0 || this.cucunverFile.id != null) {
            this.createFolder();
        } else {
            this.addFile();
        }
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgottenPageRoutingModule } from './forgotten-routing.module';

import { ForgottenPage } from './forgotten.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ForgottenPageRoutingModule,
        TranslateModule
    ],
    declarations: [ForgottenPage]
})
export class ForgottenPageModule { }

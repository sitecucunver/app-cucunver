import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UiServiceService } from '../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '../../services/login.service';

@Component({
    selector: 'app-forgotten',
    templateUrl: './forgotten.page.html',
    styleUrls: ['./forgotten.page.scss'],
})
export class ForgottenPage implements OnInit {

    forgottenUser = {
        email: '',
    }
    confirm: boolean = false;
    msg: string = ''

    constructor(
        private ui: UiServiceService,
        private translate: TranslateService,
        private loginService: LoginService
    ) { }

    ngOnInit() {
    }

    async forgotten(fLogin: NgForm) {
        if (fLogin.invalid) {
            return false;
        } else {
            await this.ui.presentLoading(this.translate.instant('messages.sending'));
        }

        const status = await this.loginService.forgotten(this.forgottenUser.email);

        if (status) {
            this.ui.closeLoading();
            this.msg = this.translate.instant("messages.forgotten5", {'email': this.forgottenUser.email})
            this.confirm = true;
        } else {
            this.ui.closeLoading();
            this.ui.alertInfo(this.translate.instant('errors.messagenotsend'));
        }
    }

    reset() {
        this.msg = '';
        this.confirm = false;
        this.forgottenUser.email = '';
    }
}

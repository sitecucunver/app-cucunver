import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '../../services/login.service';
import { UiServiceService } from '../../services/ui-service.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage {


    loginUser = {
        email: '',
        password: ''
    }

    constructor(
        private loginService: LoginService,
        private navController: NavController,
        private translate: TranslateService,
        public menu: MenuController,
        private ui: UiServiceService,
        private iab: InAppBrowser
    ) { }

    async login(fLogin: NgForm) {
        if (fLogin.invalid) {
            return false;
        } else {
            await this.ui.presentLoading(this.translate.instant('messages.login'));
        }

        const status = await this.loginService.login(this.loginUser.email, this.loginUser.password);

        if (status) {
            this.ui.closeLoading();
            this.navController.navigateRoot('/home');
        } else {
            this.ui.closeLoading();
            this.translate.get('errors.loginError').subscribe((resp: string) => {
                this.ui.alertInfo(resp);
            });
        }
    }

    ionViewWillEnter() {
        this.menu.enable(false);
    }

    ionViewDidLeave() {
        this.menu.enable(true);
    }

}

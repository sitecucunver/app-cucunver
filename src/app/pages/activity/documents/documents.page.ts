import { Component, OnInit, ViewChild } from '@angular/core';
import { Activity } from '../../../interfaces/activities';
import { ActivitiesService } from '../../../services/activities.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { DocumentsComponent } from '../../../components/documents/documents.component';

@Component({
  selector: 'app-activity-documents',
  templateUrl: './documents.page.html',
  styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {

    activity: Activity;
    @ViewChild(DocumentsComponent) documentsComponent: DocumentsComponent;

    constructor(
        private activityService: ActivitiesService,
        private navController: NavController,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.parent.parent.paramMap.get('id'));
        const activity = this.activityService.getActivity();
        if (activity == null || activity.id != id) {

            this.activityService.getActivityById(id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    this.activity = resp.result;
                } else {
                    this.navController.navigateRoot('/activities');
                }
            });

        } else {
            this.activity = activity;
        }
    }

}

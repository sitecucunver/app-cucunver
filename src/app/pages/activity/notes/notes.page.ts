import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivitiesService } from '../../../services/activities.service';
import { NavController } from '@ionic/angular';
import { Activity } from '../../../interfaces/activities';
import { ActivatedRoute } from '@angular/router';
import { NotesComponent } from '../../../components/notes/notes.component';

@Component({
    selector: 'app-activity-notes',
    templateUrl: './notes.page.html',
    styleUrls: ['./notes.page.scss'],
})
export class NotesPage implements OnInit {

    activity: Activity;
    @ViewChild(NotesComponent) notesComponent: NotesComponent;

    constructor(
        private activityService: ActivitiesService,
        private navController: NavController,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.parent.parent.paramMap.get('id'));
        const activity = this.activityService.getActivity();
        if (activity == null || activity.id != id) {

            this.activityService.getActivityById(id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    this.activity = resp.result;
                } else {
                    this.navController.navigateRoot('/activities');
                }
            });

        } else {
            this.activity = activity;
        }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentsPublicPageRoutingModule } from './documents-public-routing.module';

import { DocumentsPublicPage } from './documents-public.page';
import { ComponentsModule } from '../../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentsPublicPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [DocumentsPublicPage]
})
export class DocumentsPublicPageModule {}

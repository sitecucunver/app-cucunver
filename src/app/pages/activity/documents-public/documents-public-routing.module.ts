import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentsPublicPage } from './documents-public.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentsPublicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentsPublicPageRoutingModule {}

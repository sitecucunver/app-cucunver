import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';

import { IndexPage } from './index.page';
import { DocumentsPageModule } from '../documents/documents.module';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/activity/:id',
        pathMatch: 'full'
    },
    {
        path: ':id',
        component: IndexPage,
        children: [
            {
                path: 'data',
                loadChildren: () => import('../data/data.module').then(m => m.DataPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'notes',
                loadChildren: () => import('../notes/notes.module').then(m => m.NotesPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'messages',
                loadChildren: () => import('../messages/messages.module').then(m => m.MessagesPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'documents',
                loadChildren: () => import('../documents/documents.module').then(m => m.DocumentsPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'documents-public',
                loadChildren: () => import('../documents-public/documents-public.module').then(m => m.DocumentsPublicPageModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'attendance',
                loadChildren: () => import('../attendance/attendance.module').then(m => m.AttendancePageModule),
                canLoad: [AuthGuard]
            },
            {
                path: '',
                redirectTo: 'data',
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndexPageRoutingModule {}

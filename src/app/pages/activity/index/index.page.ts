import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Activity } from '../../../interfaces/activities';
import { ActivitiesService } from '../../../services/activities.service';
import { UiServiceService } from '../../../services/ui-service.service';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from '../../../services/helper.service';
import { ProfileService } from '../../../services/profile.service';

@Component({
    selector: 'app-index',
    templateUrl: './index.page.html',
    styleUrls: ['./index.page.scss'],
})
export class IndexPage {

    activity: Activity;

    constructor(
        private activityService: ActivitiesService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private route: ActivatedRoute,
        private helper: HelperService,
        private profileService: ProfileService
    ) { }

    async ionViewDidEnter() {
        const id = parseInt(this.route.snapshot.paramMap.get('id'));

        const resp = await this.activityService.getActivityById(id, 'schedules,affected')
        if (resp.status == 'OK') {
            this.activity = resp.result;
        }

        this.helper.getUser().then(user => {
            if (!this.activity.affecteds.some(profile => profile.profile_id == user.currentProfile.id)) {
                const profile = user.profiles.find(profile => this.activity.affecteds.some(affected => profile.id == affected.profile_id));
                if (profile) {
                    this.profileService.changeProfile(profile.id);
                }
            }
        });

    }

    async toogleRegister() {
        if (!this.activity) {
            this.activityService.getActivity();
        }
        const status = await this.activityService.activityRegistered(this.activity);
        if (status) {
            this.activityService.getActivityById(this.activity.id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    const newActivity = resp.result;
                    var msg: string;
                    if (newActivity.profile == null) {
                        msg = this.translate.instant('messages.activitycancel');
                    } else {
                        switch (newActivity.profile.registered) {
                            case 0:
                                msg = this.translate.instant('messages.activityregistered');
                                break;
                            case 1:
                                msg = this.translate.instant('messages.activitycancel2');
                                break;
                            case 2:
                                msg = this.translate.instant('messages.activityunsubscribe');
                                break;
                        }
                    }

                    this.activity = newActivity;
                    this.ui.alertInfo(msg);
                } else {
                    this.ui.alertInfo(resp.error);
                }
            }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
        }
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Activity } from '../../../interfaces/activities';
import { ActivitiesService } from '../../../services/activities.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { MessagesComponent } from '../../../components/messages/messages.component';

@Component({
  selector: 'app-activity-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {

    activity: Activity;
    @ViewChild(MessagesComponent) messagesComponent: MessagesComponent;

    constructor(
        private activityService: ActivitiesService,
        private navController: NavController,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.parent.parent.paramMap.get('id'));
        const activity = this.activityService.getActivity();
        if (activity == null || activity.id != id) {

            this.activityService.getActivityById(id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    this.activity = resp.result;
                } else {
                    this.navController.navigateRoot('/activities');
                }
            });

        } else {
            this.activity = activity;
        }
    }

}

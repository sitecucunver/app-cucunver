import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessagesPageRoutingModule } from './messages-routing.module';

import { MessagesPage } from './messages.page';
import { ComponentsModule } from '../../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';
import { MessageActionsPageModule } from '../../modals/message-actions/message-actions.module';
import { MessageThreadPageModule } from '../../modals/message-thread/message-thread.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessagesPageRoutingModule,
    ComponentsModule,
    TranslateModule,
    MessageActionsPageModule,
    MessageThreadPageModule
  ],
  declarations: [MessagesPage]
})
export class MessagesPageModule {}

import { Component, OnInit } from '@angular/core';
import { Activity, Attendance } from '../../../interfaces/activities';
import { ActivitiesService } from '../../../services/activities.service';
import { NavController } from '@ionic/angular';
import { Filter } from 'src/app/interfaces/generic';
import { UiServiceService } from '../../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {

    activity: Activity;
    attendance: Attendance[];
    hasMore: boolean;
    page: number = 1;
    searchedText: string;
    status: number = -1;

    constructor(
        private activityService: ActivitiesService,
        private navController: NavController,
        private ui: UiServiceService,
        private translate: TranslateService,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.parent.parent.paramMap.get('id'));
        const activity = this.activityService.getActivity();
        if (activity == null || activity.id != id) {

            this.activityService.getActivityById(id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    this.activity = resp.result;
                } else {
                    this.navController.navigateRoot('/activities');
                }
            });

        } else {
            this.activity = activity;
        }

        this.getAttendance(this.page);
    }

    getAttendance(page: number, event?) {
        var filter: Filter[] = [];
        if (this.status != -1) {
            filter.push({
                field: 'attendanceProfile.status',
                value: this.status
            });
        }
        this.activityService.getAttendance(this.activity.id, 20, 'attendanceProfile', filter, '-date,-hour', page).then(resp => {
            if (resp.status == 'OK') {
                if (page == 1) {
                    this.attendance = resp.result;
                } else {
                    this.attendance.push(...resp.result);
                }
                this.hasMore = resp.lastPage > page;
                this.page = ++page;
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));

    }

}

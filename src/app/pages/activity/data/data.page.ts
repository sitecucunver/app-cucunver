import { Component } from '@angular/core';
import { Activity, Schedule } from '../../../interfaces/activities';
import { ActivitiesService } from '../../../services/activities.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-activity-data',
    templateUrl: './data.page.html',
    styleUrls: ['./data.page.scss'],
})
export class DataPage {

    activity: Activity;
    scheduleMon: Schedule[];
    scheduleTue: Schedule[];
    scheduleWen: Schedule[];
    scheduleThe: Schedule[];
    scheduleFri: Schedule[];
    scheduleSat: Schedule[];
    scheduleSun: Schedule[];

    constructor(
        private activityService: ActivitiesService,
        private navController: NavController,
        private route: ActivatedRoute,
    ) { }

    ionViewDidEnter() {
        let id = parseInt(this.route.snapshot.parent.parent.paramMap.get('id'));
        const activity = this.activityService.getActivity();
        if (activity == null || activity.id != id) {

            this.activityService.getActivityById(id, 'schedules,affected').then(resp => {
                if (resp.status == 'OK') {
                    this.activity = resp.result;
                    this.setSchedule(resp.result);

                } else {
                    this.navController.navigateRoot('/activities');
                }
            });

        } else {
            this.activity = activity;
            this.setSchedule(this.activity);
        }
    }

    setSchedule(activity: Activity) {
        this.scheduleMon = activity.schedules.filter(schedule => schedule.dayOfWeek == 1);
        this.scheduleTue = activity.schedules.filter(schedule => schedule.dayOfWeek == 2);
        this.scheduleWen = activity.schedules.filter(schedule => schedule.dayOfWeek == 3);
        this.scheduleThe = activity.schedules.filter(schedule => schedule.dayOfWeek == 4);
        this.scheduleFri = activity.schedules.filter(schedule => schedule.dayOfWeek == 5);
        this.scheduleSat = activity.schedules.filter(schedule => schedule.dayOfWeek == 6);
        this.scheduleSun = activity.schedules.filter(schedule => schedule.dayOfWeek == 7);
    }

}

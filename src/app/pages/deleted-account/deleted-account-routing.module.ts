import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeletedAccountPage } from './deleted-account.page';

const routes: Routes = [
  {
    path: '',
    component: DeletedAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeletedAccountPageRoutingModule {}

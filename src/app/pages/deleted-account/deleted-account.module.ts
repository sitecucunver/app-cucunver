import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeletedAccountPageRoutingModule } from './deleted-account-routing.module';

import { DeletedAccountPage } from './deleted-account.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeletedAccountPageRoutingModule,
    TranslateModule
  ],
  declarations: [DeletedAccountPage]
})
export class DeletedAccountPageModule {}

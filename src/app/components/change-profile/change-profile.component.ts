import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../services/helper.service';
import { User } from '../../interfaces/users';
import { ProfileService } from '../../services/profile.service';
import { LoginService } from '../../services/login.service';
import { environment } from '../../../environments/environment';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-profile',
    templateUrl: './change-profile.component.html',
    styleUrls: ['./change-profile.component.scss'],
})
export class ChangeProfileComponent implements OnInit {

    url: string = environment.url;
    user: User;
    entities: User[] = [];
    entitySelected: number;
    profileSelected: number;
    avatarDefault: string = `${this.url}/avatar/avatar_thumb_default.svg`;

    constructor(
        private helper: HelperService,
        private profileService: ProfileService,
        private loginService: LoginService,
        private navController: NavController,
        private router: Router
    ) { }

    ngOnInit() {
        this.setData();
        this.profileService.changedProfile.subscribe(() => {
            this.setData();
        });
        this.loginService.logued.subscribe(() => this.setData());
    }

    setData() {
        this.helper.getUserAndSave().then(user => {
            this.user = user;
            user.profiles.forEach(profile => {

                if (this.entities.length == 0 || this.entities.every(entity => entity.id != profile.entity_id)) {
                    this.entities.push(profile.entity);
                }
            });
            if (user.profile_id) {
                this.entitySelected = this.user.currentEntity.id;
                this.profileSelected = this.user.currentProfile.id;
            }

            if (this.user.profile_id == null) {
                this.navController.navigateRoot('/noentity');
            } else if (!this.user.currentEntity.hasPlan) {
                this.navController.navigateRoot('/entity-no-plan');
            } else if (this.user.role_id == 3 || this.user.role_id == 4 || this.user.currentProfile.isEditor) {
                this.navController.navigateRoot('/under-contruction');
            } else {
                if (this.router.url == '/noentity' || this.router.url == '/entity-no-plan' || this.router.url == '/under-contruction') {
                    this.navController.navigateRoot('/home');
                }
            }

        });
    }

    change(section) {
        if (section == 'entity') {
            this.profileSelected = this.user.profiles.find(profile => this.entitySelected == profile.entity.id).id;
        } else {
            if (this.user.currentProfile.id != this.profileSelected) {
                this.profileService.changeProfile(this.profileSelected);
            }
        }
    }

}

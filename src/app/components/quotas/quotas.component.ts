import { Component, Input, OnInit } from '@angular/core';
import { ProfileQuotaYear } from '../../interfaces/treasury';
import { TreasuryService } from '../../services/treasury.service';
import { Filter } from '../../interfaces/generic';
import { UiServiceService } from '../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { ProfileService } from '../../services/profile.service';
import { IonModal } from '@ionic/angular';

@Component({
    selector: 'app-quotas',
    templateUrl: './quotas.component.html',
    styleUrls: ['./quotas.component.scss'],
})
export class QuotasComponent implements OnInit {

    @Input() title: string;
    @Input() active: boolean;
    page: number = 1;
    years: number[];
    pqys: ProfileQuotaYear[];
    year: number = new Date().getFullYear();;
    hasMore: boolean;
    searchText: string;

    constructor(
        private trasuryService: TreasuryService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private profileService: ProfileService
    ) { }


    ngOnInit() {
        this.getQuotas(this.page);
        this.profileService.changedProfile.subscribe(() => this.getQuotas(1));
    }

    getQuotas(page, event?) {
        const filterQuota = this.filter(this.year, this.searchText);
        this.trasuryService.getProfileQuotaYears('quota,profile,pq', 20, filterQuota, '-updated_at', page).then(resp => {
            if (resp.status == 'OK') {
                this.years = resp.years;
                if (this.years.length > 0) {
                    if (this.years.includes(this.year)) {
                        this.pqys = resp.result;
                        this.hasMore = resp.lastPage > page;
                        this.page = page + 1;
                    } else {
                        this.year = this.years[0];
                        this.getQuotas(page);
                    }
                } else {
                    this.pqys = resp.result;
                    this.hasMore = false;
                    this.page = page;
                }

            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
    }

    filter(year: number, text?: string): Filter[] {
        let filter: Filter[] = [
            {
                field: 'year',
                value: year
            },
        ];

        if (text) {
            filter.push({ field: 'quota.title', value: text })
        }
        return filter;
    }

    changeDate(event, modal: IonModal) {
        this.year = parseInt(event.detail.value);
        this.getQuotas(1);
        modal.dismiss();
    }

    reset(modal: IonModal) {
        this.year = this.years[0] ?? new Date().getFullYear();
        modal.dismiss();
    }
}

import { Component, Input, OnInit } from '@angular/core';
import { UiServiceService } from 'src/app/services/ui-service.service';
import { News, Tag, NewsTag } from '../../interfaces/news';
import { NewsService } from '../../services/news.service';
import { TranslateService } from '@ngx-translate/core';
import { Filter } from 'src/app/interfaces/generic';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { ProfileService } from '../../services/profile.service';
import { HelperService } from '../../services/helper.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-news-container',
    templateUrl: './news-container.component.html',
    styleUrls: ['./news-container.component.scss'],
})
export class NewsContainerComponent implements OnInit {

    @Input() title: string;
    @Input() inHome: boolean;
    @Input() listShow: number = 20;
    newsList: News[];
    tags: Tag[];
    page: number = 1;
    searchedText: string;
    hasMore: boolean;
    newsTagId: number = 0;

    constructor(
        private newsService: NewsService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private iab: InAppBrowser,
        private profileService: ProfileService,
        private helper: HelperService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.getNews(this.page)
        this.profileService.changedProfile.subscribe(() => this.getNews(1));
        this.route.queryParams.subscribe(async params => {
            if (params.id) {
                const user = await this.helper.getUser();
                this.newsService.getNewsById(params.id).then(resp => {
                    if (resp.status == 'OK') {
                        if (resp.result.entity_id != user.entity_id) {
                            const profile = user.profiles.find(profile => profile.entity_id == resp.result.entity_id);
                            this.profileService.changeProfile(profile.id);
                        }
                    }
                });
            }
        });
    }


    getNews(page: number, event?) {
        var filter: Filter[] = [];
        if (this.searchedText != null) {
            filter.push({
                field: 'title,description,link',
                value: this.searchedText
            });
        }

        if (this.newsTagId != 0) {
            filter.push({
                field: 'tags.tag.id',
                value: this.newsTagId
            })
        }
        this.newsService.getNews(this.listShow, 'tags', filter, '-date,-id', page).then(resp => {
            if (resp.status == 'OK') {
                if (page == 1) {
                    this.newsList = resp.result;
                    if (filter.length == 0) {
                        var newsTags: NewsTag[] = [];
                        this.newsList.forEach(news => {
                            newsTags.push(...news.tags);
                        });
                        this.tags = [...new Map(newsTags.map(newsTag => [newsTag.tag.id, newsTag.tag])).values()];
                    }
                } else {
                    this.newsList.push(...resp.result);
                }
                this.hasMore = resp.lastPage > this.page;
                this.page = ++page;
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }

        }).catch(() => {
            this.ui.alertInfo(this.translate.instant('errors.genericerror'))
        });

    }

    openLink(url) {
        this.iab.create(url, '_system', 'location=yes');
    }

}

import { Component, Input, OnInit } from '@angular/core';
import { ProfileQuotaYear } from '../../interfaces/treasury';

@Component({
    selector: 'app-quota',
    templateUrl: './quota.component.html',
    styleUrls: ['./quota.component.scss'],
})
export class QuotaComponent implements OnInit {

    @Input() pqy: ProfileQuotaYear;
    pqyConverted = [];

    constructor() { }

    ngOnInit() {
        for (let index = 1; index <= 12; index++) {
            if (this.pqy['month' + index + '_amount'] != 0 && !this.pqy['month' + index + '_disabled']) {
                let arrayMonth = {
                    amount: this.pqy['month' + index + '_amount'],
                    disabled: this.pqy['month' + index + '_disabled'],
                    paid: this.pqy['month' + index + '_paid'],
                    paidImg: this.pqy['month' + index + '_paidImg'],
                    paymenttype: this.pqy['month' + index + '_paymenttype'],
                    month: 'month' + index
                };
                this.pqyConverted.push( arrayMonth );
            }
        }
    }

}

import { Component, Input, OnInit } from '@angular/core';
import { DocumentsService } from '../../services/documents.service';
import { Filter } from '../../interfaces/generic';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../services/ui-service.service';
import { CucunverFile } from 'src/app/interfaces/files';
import { ActionSheetButton, ActionSheetController, ModalController } from '@ionic/angular';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { DocumentActionsPage } from '../../pages/modals/document-actions/document-actions.page';
import { ProfileService } from '../../services/profile.service';
@Component({
    selector: 'app-documents-cucunver',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
})
export class DocumentsComponent implements OnInit {

    @Input() title: string;
    @Input() section: string;
    @Input() id: number;
    page: number = 1;
    cucunverFiles: CucunverFile[];
    hasActions: boolean;
    hasMore: boolean;
    searchedText: string;
    cucunverFile: CucunverFile;
    folder: CucunverFile;

    constructor(
        private documentService: DocumentsService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private actionSheetController: ActionSheetController,
        private iab: InAppBrowser,
        private modalController: ModalController,
        private profileService: ProfileService
    ) { }

    ngOnInit() {
        this.setDefault();
        this.getDocuments(this.page);
        this.profileService.changedProfile.subscribe(() => {
            this.setDefault();
            this.getDocuments(1);
        });
    }

    setDefault() {
        this.cucunverFile = {
            name: '',
            type: 0,
            affected_id: this.id,
            section: this.section,
        }
    }

    getDocuments(page: number, parent_id?: number, event?) {
        var filter: Filter[];
        if (parent_id) {
            filter = [{
                field: 'parent_id',
                value: parent_id
            }];
        }
        if (this.searchedText) {
            filter = [{
                field: 'name',
                value: this.searchedText
            }];
        }
        this.documentService.getDocuments(this.section, this.id, 'creator,parent.parent', 20, filter, '-updated_at', page).then(resp => {
            if (resp.status == 'OK') {
                this.checkParent(parent_id);
                this.cucunverFiles = resp.result;
                this.cucunverFile.parent_id = parent_id;
                this.hasActions = resp.permit;
                this.hasMore = resp.lastPage > page;
                this.page = ++page
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));

    }

    async actions(file: CucunverFile) {
        var actionButtons: ActionSheetButton[] = [
            {
                text: this.translate.instant('messages.open'),
                icon: 'open',
                handler: () => {
                    if (file.type == 1) {
                        this.getDocuments(1, file.id);
                    } else {
                        this.iab.create(file.filePath, '_system', 'location=yes');
                    }
                },
            }
        ];
        if (this.hasActions) {
            const buttons: ActionSheetButton[] = [
                {
                    text: this.translate.instant('messages.edit'),
                    icon: 'pencil',
                    handler: () => {
                        this.edit(file);
                    },
                },
                {
                    text: this.translate.instant('messages.delete'),
                    role: 'destructive',
                    icon: 'trash',
                    handler: () => {
                        this.delete(file);
                    },
                },
            ];
            actionButtons.push(...buttons);
        }

        const actionSheet = await this.actionSheetController.create({
            buttons: actionButtons,
        });
        await actionSheet.present();
    }

    edit(file: CucunverFile) {
        this.cucunverFile = file;
        this.openModal();
    }

    async delete(file: CucunverFile) {
        const status = await this.documentService.deleteFile(file);
        if (status) {
            this.cucunverFiles = this.cucunverFiles.filter(cucunverFile => cucunverFile.id != file.id);
        }
    }

    async openModal() {
        const modal = await this.modalController.create({
            component: DocumentActionsPage,
            id: 'document-actions',
            componentProps: {
                cucunverFile: this.cucunverFile,
                hasActions: this.hasActions
            }
        });

        await modal.present();

        const {data} = await modal.onDidDismiss();
        if (data && data.saved) {
            this.getDocuments(1, this.folder ? this.folder.id : null);
        }
        this.setDefault();
    }

    checkParent(parent_id: number) {
        if (this.folder == null || this.folder.id != parent_id) {
            if (parent_id) {
                //Esta entrando del root a una carpeta
                if (!this.folder) {
                    this.folder = this.cucunverFiles.find(file => file.id == parent_id);
                } else {
                    //Esta moviendose por carpetas hijas
                    if (this.cucunverFiles.some(file => file.id == parent_id)) {
                        this.folder = this.cucunverFiles.find(file => file.id == parent_id);
                    } else {
                        this.folder = this.folder.parent ?? null;
                    }
                }
            } else {
                //Esta retrocediendo hasta el root
                this.folder = null;
            }
        }
    }
}

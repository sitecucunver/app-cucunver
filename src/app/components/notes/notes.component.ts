import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note } from '../../interfaces/generic';
import { NoteService } from '../../services/note.service';
import { UiServiceService } from '../../services/ui-service.service';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { NoteActionPage } from '../../pages/modals/note-actions/note-action.page';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {

    @Input() title: string;
    @Input() section: string;
    @Input() table_id: number;
    @Output() infinite = new EventEmitter();
    page: number = 1;
    hasMore: boolean;
    notes: Note[];
    note: Note;

    constructor(
        private noteService: NoteService,
        private ui: UiServiceService,
        private modalController: ModalController,
        private translate: TranslateService,
        private actionSheetController: ActionSheetController,
        private iab: InAppBrowser,
        private profileService: ProfileService
    ) { }

    ngOnInit() {
        this.setData();
        this.profileService.changedProfile.subscribe(() => this.getNotes(this.section, this.table_id, 1));
    }

    setData(event?) {
        this.getNotes(this.section, this.table_id, this.page);
        this.noteService.noteUpdate.subscribe(resp => {
            let updateItem = this.notes.find(note => note.id == resp.id);
            this.notes.splice(this.notes.indexOf(updateItem), 1, resp);
        });

        if (event) {
            event.target.complete();
        }
    }

    getNotes(section: string, table_id: number, page: number) {
        this.noteService.getNotes(section, table_id, 'editor,profile', null, 10, '-id', page).then(resp => {
            if (resp.status == 'OK') {
                if (page == 1) {
                    this.notes = resp.result.reverse();
                } else {
                    this.notes.unshift(...resp.result.reverse());
                }
                this.hasMore = resp.lastPage > page;
                this.page = ++page;
            } else {
                this.ui.alertInfo(resp.error);
            }
        });
    }

    async addNote() {
        this.note = {
            text: '',
            file: null,
            tableName: this.section,
            table_id: this.table_id
        };
        const modal = await this.modalController.create({
            component: NoteActionPage,
            componentProps: {
                note: this.note,
                title: this.translate.instant('messages.addNote'),
            }
        });
        await modal.present();

        let { data } = await modal.onDidDismiss();

        if (data && data.saved) {
            this.getNotes(this.section, this.table_id, 1);
        }
    }

    async actions(note: Note) {
        const actionSheet = await this.actionSheetController.create({
            buttons: [
                {
                    text: this.translate.instant('messages.notedelete'),
                    role: 'destructive',
                    icon: 'trash',
                    handler: () => {
                        this.delete(note.id);
                    },
                },
                {
                    text: this.translate.instant('messages.edit'),
                    icon: 'pencil',
                    handler: () => {
                        this.edit(note);
                    },
                },
            ],
        });
        await actionSheet.present();
    }

    async delete(id: number) {
        const role = await this.ui.confirmUi(this.translate.instant('messages.notedelete'), this.translate.instant('messages.notedeletemsg'));

        if (role == 'confirm') {
            const status = await this.noteService.deleteNote(id);
            if (status) {
                this.notes = this.notes.filter(note => note.id != id);
            }
        }
    }

    async edit(note: Note) {
        const modal = await this.modalController.create({
            component: NoteActionPage,
            componentProps: {
                note,
                title: this.translate.instant('messages.editNote')
            }
        });
        await modal.present();
    }

    openFile(event: Event, path: string) {
        event.stopPropagation();
        this.iab.create(path, '_system', 'location=yes');
    }
}

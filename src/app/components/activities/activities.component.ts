import { Component, Input, AfterViewInit } from '@angular/core';
import { Activity } from '../../interfaces/activities';
import { ActivitiesService } from '../../services/activities.service';
import { UiServiceService } from '../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Filter } from 'src/app/interfaces/generic';
import { NavController } from '@ionic/angular';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-activities',
    templateUrl: './activities.component.html',
    styleUrls: ['./activities.component.scss'],
})
export class ActivitiesComponent implements AfterViewInit {

    @Input() id: number;
    @Input() title: string;
    @Input() inHome: boolean;
    @Input() listShow: number = 20;
    activities: Activity[];
    page: number = 1;
    searchedText: string;
    hasMore: boolean;

    constructor(
        private activitiesService: ActivitiesService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private navController: NavController,
        private profileService: ProfileService
    ) { }

    ngAfterViewInit(): void {
        this.getActivities(this.page);
        this.profileService.changedProfile.subscribe(() => this.getActivities(1));
    }

    getActivities(page: number, event?) {
        var filter: Filter[] = [];
        if (this.searchedText) {
            filter.push({
                field: 'title',
                value: this.searchedText
            });
        }
        this.activitiesService.getActivities(this.listShow, 'schedules,affected', filter, '-updated_at', page).then(resp => {
            if (resp.status == 'OK') {
                if (page == 1) {
                    this.activities = resp.result;
                } else {
                    this.activities.push(...resp.result);
                }
                this.hasMore = this.page < resp.lastPage;
                this.page = ++page;
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));

    }

    openActivity(activity: Activity) {
        const id = activity.id;
        this.navController.navigateForward(`/activity/${id}`);
    }
}

import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Workplace } from '../../interfaces/users';

@Component({
    selector: 'app-workplaces',
    templateUrl: './workplaces.component.html',
    styleUrls: ['./workplaces.component.scss'],
})
export class WorkplacesComponent implements OnInit {

    @Input() workplaces: Workplace[];
    constructor(private popoverController: PopoverController) { }

    ngOnInit() {}

    onClick(workplace: Workplace) {
        this.popoverController.dismiss({
            workplace
        });
    }

}

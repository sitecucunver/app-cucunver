import { Component, Input, OnInit } from '@angular/core';
import { HelperService } from '../../services/helper.service';
import { TranslateService } from '@ngx-translate/core';
import { BalanceGrafic } from '../../interfaces/treasury';
import { TreasuryService } from '../../services/treasury.service';
import { UiServiceService } from '../../services/ui-service.service';
import { ProfileService } from '../../services/profile.service';

import {
    ApexAxisChartSeries,
    ApexChart,
    ApexXAxis,
    ApexDataLabels,
    ApexStroke,
    ApexTooltip,
    ApexMarkers,
    ApexNoData,
} from "ng-apexcharts";

export type ChartOptions = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    xaxis: ApexXAxis;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    tooltip: ApexTooltip;
    markers: ApexMarkers;
    colors: string[];
    noData: ApexNoData;
};

@Component({
    selector: 'app-apexchart',
    templateUrl: './apexchart.component.html',
    styleUrls: ['./apexchart.component.scss'],
})
export class ApexchartComponent implements OnInit {

    balance: BalanceGrafic;
    @Input() title: string;
    public chartOptions: Partial<ChartOptions>;

    constructor(
        private helperService: HelperService,
        private translate: TranslateService,
        private treasuryService: TreasuryService,
        private ui: UiServiceService,
        private profileService: ProfileService
    ) { }

    ngOnInit(): void {
        this.setData();
        this.profileService.changedProfile.subscribe(() => this.setData());
    }

    setData() {
        this.helperService.getApexChartLocale(this.translate.currentLang).subscribe(locale => {
            this.treasuryService.getBalanceGrafic().then(resp => {
                if (resp.status == 'OK') {
                    this.balance = resp.result;
                    this.chartOptions = {
                        series: [
                            {
                                name: this.translate.instant('messages.expenses'),
                                data: this.balance.incomes
                            },
                            {
                                name: this.translate.instant('messages.incomes'),
                                data: this.balance.expenses
                            }
                        ],
                        chart: {
                            width: '100%',
                            height: '300px',
                            type: 'area',
                            toolbar: {
                                show: true,
                                tools: {
                                    download: false,
                                    selection: false,
                                    zoom: true,
                                    zoomin: true,
                                    zoomout: true,
                                    pan: false,
                                    reset: true,
                                    customIcons: []
                                }
                            },
                            locales: [locale],
                            defaultLocale: this.translate.currentLang
                        },
                        dataLabels: {
                            enabled: false
                        },
                        markers: {
                            size: 4,
                        },
                        stroke: {
                            curve: 'smooth'
                        },
                        xaxis: {
                            type: 'datetime',
                        },
                        tooltip: {
                            x: {
                                format: 'dd/MM/yy'
                            },
                            y: {
                                formatter: function (y) {
                                    return y.toFixed(2) + " €";
                                }
                            },
                        },
                        colors: ['#cb154a', '#2db088'],
                        noData: {
                            text: "Sin datos"
                        },
                    };
                } else {
                    this.ui.alertInfo(resp.error);
                }
            }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
        });
    }
}

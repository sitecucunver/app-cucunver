import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../interfaces/users';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { EntityService } from '../../services/entity.service';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../services/ui-service.service';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-entity',
    templateUrl: './entity.component.html',
    styleUrls: ['./entity.component.scss'],
})
export class EntityComponent implements OnInit {

    entity: User;
    @Input() title: string;
    @Input() isEntitySection: boolean = false;

    constructor(
        private iab: InAppBrowser,
        private entityService: EntityService,
        private translate: TranslateService,
        private ui: UiServiceService,
        private profileService: ProfileService
    ) { }

    ngOnInit() {
        this.setData();
        this.profileService.changedProfile.subscribe(() => this.setData());
    }

    setData(event?) {
        this.entityService.getEntity('socialnetworks').then(resp => {
            if (resp.status == 'OK') {
                this.entity = resp.result;
            } else {
                this.ui.alertInfo(resp.error)
            }

            if(event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));

    }

    open(url: string) {
        this.iab.create(url, '_system', 'location=yes');
    }

}

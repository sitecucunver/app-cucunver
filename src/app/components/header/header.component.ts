import { Component, Input, AfterViewInit, ViewChild } from '@angular/core';
import { HelperService } from '../../services/helper.service';
import { User } from '../../interfaces/users';
import { IonPopover, NavController } from '@ionic/angular';
import { LoginService } from '../../services/login.service';
import { ProfileService } from '../../services/profile.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements AfterViewInit {

    url: string = environment.url;
    @Input() title: string = 'home';
    avatar: string = `${this.url}/avatar/avatar_thumb_default.svg`;
    user: User;
    @ViewChild(IonPopover) popover: IonPopover;

    constructor(
        private helperService: HelperService,
        private navController: NavController,
        private loginService: LoginService,
        private profileService: ProfileService
    ) { }

    async ngAfterViewInit() {
        if (this.title == 'myprofile') {
            this.user = await this.helperService.getUser();
        } else {
            this.user = await this.helperService.getUserAndSave();
        }
        if (this.user.profile_id) {
            this.avatar = this.user.currentProfile.avatar;
        }

        this.profileService.changedProfile.subscribe(async () => {
            this.user = await this.helperService.getUserAndSave();
            this.avatar = this.user.currentProfile.avatar;
        });
    }

    logout(popover: IonPopover) {
        popover.dismiss();
        this.loginService.logout();
    }

    configuration(popover: IonPopover) {
        popover.dismiss();
        this.navController.navigateRoot('/configuration');

    }

    onClick(event) {
        this.popover.present(event)

    }
}
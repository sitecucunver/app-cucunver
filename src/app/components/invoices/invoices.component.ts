import { Component, OnInit, Input } from '@angular/core';
import { Invoice } from '../../interfaces/treasury';
import { Filter } from '../../interfaces/generic';
import { TreasuryService } from '../../services/treasury.service';
import { UiServiceService } from '../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent implements OnInit {

    @Input() title: string;
    @Input() active: boolean;
    searchedText: string;
    invoices: Invoice[];
    hasMore: boolean;
    page: number = 1;

    constructor(
        private treasuryService: TreasuryService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private iab: InAppBrowser
    ) { }

    ngOnInit() {
        this.getInvoices(this.page);
    }

    getInvoices(page: number, event?) {
        var filter: Filter[];
        if (this.searchedText) {
            filter = [{
                field: 'number,serial,amount,date',
                value: this.searchedText
            }];
        }
        this.treasuryService.getInvoice('', 20, filter, '-date,-number', page).then(resp => {
                if (resp.status == 'OK') {
                    this.invoices = resp.result;
                    this.hasMore = this.page < resp.lastPage;
                    this.page = ++page;
                } else {
                    this.ui.alertInfo(resp.error);
                }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
    }

    showBill(invoice: Invoice) {
        this.iab.create(invoice.billUrl, '_system', 'location=yes');
    }
}

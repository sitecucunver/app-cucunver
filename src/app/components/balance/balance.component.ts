import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Balance } from '../../interfaces/treasury';
import { ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { UiServiceService } from '../../services/ui-service.service';

@Component({
    selector: 'app-balance',
    templateUrl: './balance.component.html',
    styleUrls: ['./balance.component.scss'],
})
export class BalanceComponent implements OnInit {

    @Input() balance: Balance;
    @Output() pay = new EventEmitter<Balance>();
    showPartial: boolean;

    constructor(
        private actionSheet: ActionSheetController,
        private translate: TranslateService,
        private ui: UiServiceService
    ) { }

    ngOnInit() {
        this.showPartial = (!(this.balance.fractional == 0 || (this.balance.fractional == 2 && this.balance.status == 1) || (this.balance.fractional == 2 && this.balance.status == 3) || this.balance.fractional == 4 || this.balance.fractional == 6)) ?? false
    }

    async action() {
        if (!this.showPartial && this.balance.status == 6) {
            const actionSheet = await this.actionSheet.create({
                buttons: [{
                    text: this.translate.instant('messages.incomepay'),
                    icon: 'wallet',
                    data: 'Data value',
                    handler: () => {
                        this.confirmPay();
                    }
                }, {
                    text: 'Cancel',
                    icon: 'close',
                    role: 'cancel',
                }]
            });
            await actionSheet.present();
        }

    }

    async confirmPay() {
        const role = await this.ui.confirmUi(this.translate.instant('messages.confirmedpaytitle'), this.translate.instant('messages.confirmpaymsg', {'income': this.balance.title}));
        if (role == 'confirm') {
            this.pay.emit(this.balance);
        }
    }

}

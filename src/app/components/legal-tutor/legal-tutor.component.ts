import { Component, OnInit, Input } from '@angular/core';
import { LegalTutor } from '../../interfaces/users';

@Component({
    selector: 'app-legal-tutor',
    templateUrl: './legal-tutor.component.html',
    styleUrls: ['./legal-tutor.component.scss'],
})
export class LegalTutorComponent implements OnInit {

    @Input() legalTutor: LegalTutor;
    @Input() isYounger: boolean;

    constructor() { }

    ngOnInit() { }

}

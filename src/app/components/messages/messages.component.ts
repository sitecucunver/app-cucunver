import { Component, Input, OnInit } from '@angular/core';
import { Filter } from 'src/app/interfaces/generic';
import { MessagesService } from 'src/app/services/messages.service';
import { UiServiceService } from 'src/app/services/ui-service.service';
import { Message } from '../../interfaces/messages';
import { TranslateService } from '@ngx-translate/core';
import { ActionSheetButton, ActionSheetController, ModalController } from '@ionic/angular';
import { MessageThreadPage } from '../../pages/modals/message-thread/message-thread.page';
import { MessageActionsPage } from '../../pages/modals/message-actions/message-actions.page';
import { HelperService } from '../../services/helper.service';
import { User } from '../../interfaces/users';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {

    messages: Message[];
    @Input() title: string;
    @Input() activity_id: number = null;
    @Input() inHome: boolean = false;
    @Input() listShow: number = 20;
    page: number = 1;
    hasMore: boolean;
    searchedText: string;
    visible: number = 1;
    user: User;

    constructor(
        private messageService: MessagesService,
        private ui: UiServiceService,
        private translate: TranslateService,
        private actionSheetController: ActionSheetController,
        private modalController: ModalController,
        private helper: HelperService,
        private route: ActivatedRoute,
        private profileService: ProfileService
    ) { }

    async ngOnInit() {
        this.getMessages(this.page);
        this.user = await this.helper.getUser();

        this.route.queryParams.subscribe(async params => {
            if (params.id) {
                const resp = await this.messageService.getMessage(params.id, 'getParent,receivers,receiver');
                if (resp.result.receiver == null) {
                    const profile = this.user.profiles.find(profile => resp.result.receivers.some(affected => profile.id == affected.profile_id));
                    this.profileService.changeProfile(profile.id);
                }
                if (resp) {
                    if (resp.result.parent) {
                        this.viewMessage(resp.result.parent);

                    } else {
                        this.viewMessage(resp.result);

                    }
                }
            }
        });

        this.profileService.changedProfile.subscribe(async () => {
            this.getMessages(1);
            this.user = await this.helper.getUser();
        });
    }

    getMessages(page: number, event?) {
        var filter: Filter[] = [
            {
                field: 'visible',
                value: this.visible
            }
        ];

        if (this.searchedText) {
            filter.push({
                field: 'title',
                value: this.searchedText
            });
        }

        if (this.activity_id) {
            filter.push({
                field: 'activity_id',
                value: this.activity_id
            });
        }

        this.messageService.getMessages(20, 'sender,receiver', filter, '-updated_at', page).then(resp => {
            if (resp.status == 'OK') {
                if (page == 1) {
                    this.messages = resp.result;
                } else {
                    this.messages.push(...resp.result);
                }
                // this.messages.sort((a,b) => {
                //     return b.lastUpdate - a.lastUpdate;
                // });
                this.hasMore = this.page < resp.lastPage;
                this.page = ++page
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => {
            this.ui.alertInfo(this.translate.instant('errors.genericerror'));
        });

    }

    async actions(message: Message) {
        var actionButtons: ActionSheetButton[] = [
            {
                text: this.translate.instant('messages.see'),
                icon: 'eye-outline',
                handler: () => {
                    this.viewMessage(message);
                },
            },
        ];

        if (message.receiver.read) {
            actionButtons.push({
                text: this.translate.instant('messages.notificationcheckon'),
                icon: 'mail-outline',
                handler: () => {
                    this.toogleRead(message);
                },
            });
        } else {
            actionButtons.push({
                text: this.translate.instant('messages.notificationcheckoff'),
                icon: 'mail-open-outline',
                handler: () => {
                    this.toogleRead(message);
                },
            });
        }

        if (message.receiver.visible) {
            actionButtons.push({
                text: this.translate.instant('messages.archive'),
                role: 'destructive',
                icon: 'close-circle-outline',
                handler: () => {
                    this.toogleArchive(message);
                },
            });
        } else {
            actionButtons.push({
                text: this.translate.instant('messages.show'),
                role: 'destructive',
                icon: 'close-circle-outline',
                handler: () => {
                    this.toogleArchive(message);
                },
            });
        }

        const actionSheet = await this.actionSheetController.create({
            buttons: actionButtons,
        });

        await actionSheet.present();
    }

    async toogleArchive(message: Message) {
        const status = await this.messageService.toogleArchive(message);
        if (status) {
            this.getMessages(1);
        }
    }

    async toogleRead(message: Message) {
        const status = await this.messageService.toogleRead(message);
        if (status) {
            this.getMessages(1);
        }
    }

    async viewMessage(message: Message) {
        const modal = await this.modalController.create({
            component: MessageThreadPage,
            componentProps: {
                message,
                title: message.title
            },
            id: 'message-thread'
        });
        await modal.present();

        if (modal.onDidDismiss()) {
            this.getMessages(1);
        }
    }

    async addMessage() {
        const modal = await this.modalController.create({
            component: MessageActionsPage,
            componentProps: {
                title: this.translate.instant('titles.sendto') + ' ' + this.user.currentEntity.firstName,
                message: {
                    activity_id: this.activity_id
                },
                action: 'message-new',
                canAttach: true,
            },
            id: 'message-actions'
        });
        await modal.present();

        let { data } = await modal.onDidDismiss();

        if (data && data.saved) {
            this.getMessages(1);
        }
    }
}

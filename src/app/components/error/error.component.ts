import { Component, Input, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {

    @Input() control: NgModel;
    @Input() submitted: boolean;

    constructor() { }

    ngOnInit() {}

}

import { Component, Input, OnInit } from '@angular/core';
import { Paymethod } from '../../interfaces/treasury';
import { ProfileService } from '../../services/profile.service';
import { UiServiceService } from '../../services/ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { TreasuryService } from '../../services/treasury.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { PaymethodActionsPage } from '../../pages/modals/paymethod-actions/paymethod-actions.page';
import { Filter } from '../../interfaces/generic';

@Component({
    selector: 'app-paymethods',
    templateUrl: './paymethods.component.html',
    styleUrls: ['./paymethods.component.scss'],
})
export class PaymethodsComponent implements OnInit {

    @Input() title: string;
    @Input() active: boolean;
    paymethods: Paymethod[];
    searchedText: string;
    page: number = 1;
    hasMore: boolean;

    constructor(
        private ui: UiServiceService,
        private translate: TranslateService,
        private treasuryService: TreasuryService,
        private actionSheetController: ActionSheetController,
        private modalController: ModalController,
        private profileService: ProfileService
    ) { }

    async ngOnInit() {
        this.getPaymethods(this.page);
        this.treasuryService.paymethodNew.subscribe(resp => {
            this.paymethods.unshift(resp);
        });
        this.treasuryService.paymethodEdit.subscribe(resp => {
            let updateItem = this.paymethods.find(method => method.id == resp.id);
            this.paymethods.splice(this.paymethods.indexOf(updateItem), 1, resp);
        });

        this.profileService.changedProfile.subscribe(() => this.getPaymethods(1));
    }

    getPaymethods(page: number, event?) {
        var filter: Filter[];
        if (this.searchedText) {
            filter = [{
                field: 'number',
                value: this.searchedText
            }];
            page = null; //Obtener todos los métodos de pago al buscar
        }
        this.treasuryService.getPaymethods('profile', 20, filter, '-updated_at', page).then(resp => {
            if (resp.status == 'OK') {
                this.paymethods = resp.result;
                this.hasMore = this.page < resp.lastPage;
                this.page = ++page;
            } else {
                this.ui.alertInfo(resp.error);
            }

            if (event) {
                event.target.complete();
            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));

    }


    async actions(paymethod: Paymethod) {
        var buttons: any[] = [
            {
                text: this.translate.instant('messages.deletepaymethod'),
                role: 'destructive',
                icon: 'trash',
                handler: () => {
                    this.delete(paymethod);
                },
            },
        ];

        if (paymethod.type == 3 || paymethod.type == 4) {
            buttons.push({
                text: this.translate.instant('messages.edit'),
                icon: 'pencil',
                handler: () => {
                    this.edit(paymethod);
                },
            });
        }

        if (paymethod.default == '') {
            buttons.push({
                text: this.translate.instant('messages.paymentmethodsdefault'),
                icon: 'star',
                handler: () => {
                    this.setAsDefault(paymethod);
                },
            });
        }
        const actionSheet = await this.actionSheetController.create({
            buttons,
        });
        await actionSheet.present();
    }

    search() {
        this.getPaymethods(1, this.searchedText);
    }

    async delete(paymethod: Paymethod) {
        var msg: string;
        if (paymethod.type == 1 || paymethod.type == 2) {
            msg = this.translate.instant('messages.deletepaymethod3', { paymethod: this.translate.instant('messages.paymenttype' + paymethod.type) });
        } else {
            msg = this.translate.instant('messages.deletepaymethod2', { paymethod: paymethod.numberFormat });
        }
        const role = await this.ui.confirmUi(this.translate.instant('messages.deletepaymethod'), msg);
        if (role == 'confirm') {
            const status = await this.treasuryService.paymethodDelete(paymethod);

            if (status) {
                this.getPaymethods(1);
            }
        }
    }

    async edit(paymethod: Paymethod) {
        const modal = await this.modalController.create({
            component: PaymethodActionsPage,
            componentProps: {
                action: 'edit',
                paymethod
            },
            id: 'paymethod-action'
        });
        await modal.present();
    }

    async setAsDefault(paymethod: Paymethod) {
        this.ui.presentLoading(this.translate.instant('messages.saving'));
        const status = await this.treasuryService.paymethodSetDefault(paymethod);

        if (status) {
            this.getPaymethods(1);
        }
        this.ui.closeLoading();
    }
}

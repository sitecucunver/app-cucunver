import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Profile, User } from '../../interfaces/users';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-profile-changes',
    templateUrl: './profile-changes.component.html',
    styleUrls: ['./profile-changes.component.scss'],
})
export class ProfileChangesComponent {

    @Input() profile: Profile;
    @Input() user: User;
    @Output() editProfile = new EventEmitter<boolean>();
    showChanges = true;

    constructor(
        private profileService: ProfileService,
    ) { }

    edit() {
        this.showChanges = false;
        //Cambiar todas los atributos por los _tmp
        this.profile.firstName = this.profile.firstName_tmp;
        this.profile.lastName = this.profile.lastName_tmp;
        this.profile.identification_type = this.profile.identification_type_tmp;
        this.profile.nif = this.profile.nif_tmp;
        this.profile.email = this.profile.email_tmp;
        this.profile.birthday = this.profile.birthday_tmp;
        this.profile.gender = this.profile.gender_tmp;
        this.profile.address = this.profile.address_tmp;
        this.profile.country = this.profile.country_tmp;
        this.profile.province = this.profile.province_tmp;
        this.profile.town = this.profile.town_tmp;
        this.profile.pc = this.profile.pc;
        this.profile.phone2 = this.profile.phone2_tmp;
        this.profile.phone = this.profile.phone_tmp;
        if (this.user.showWorkplace) {
            this.profile.addressed = this.profile.addressed_tmp;
            this.profile.nacionality = this.profile.nacionality_tmp;
            this.profile.workplace = this.profile.workplace_tmp;
            this.profile.charge = this.profile.charge_tmp;
            this.profile.unit = this.profile.unit_tmp;
            this.profile.speciality = this.profile.speciality_tmp;
        }

        if (this.user.showNShops) {
            this.profile.n_shops = this.profile.n_shops_tmp;
        }

        if (this.user.showSpecialityEESTO) {
            this.profile.speciality = this.profile.speciality_tmp;
        }

        if (this.user.showIsNurse) {
            this.profile.is_nurse = this.profile.is_nurse_tmp;
        }
        this.editProfile.emit(true);
    }

    cancel() {
        this.profileService.cancelPetition();
    }
}

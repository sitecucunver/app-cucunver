import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { ApexchartComponent } from './apexchart/apexchart.component';
import { TranslateModule } from '@ngx-translate/core';
import { ActivitiesComponent } from './activities/activities.component';
import { MessagesComponent } from './messages/messages.component';
import { NewsContainerComponent } from './news-container/news-container.component';
import { NgApexchartsModule } from 'ng-apexcharts'
import { EntityComponent } from './entity/entity.component';
import { DocumentsComponent } from './documents/documents.component';
import { WorkplacesComponent } from './workplaces/workplaces.component';
import { ProfileChangesComponent } from './profile-changes/profile-changes.component';
import { LegalTutorComponent } from './legal-tutor/legal-tutor.component';
import { ErrorComponent } from './error/error.component';
import { NotesComponent } from './notes/notes.component';
import { QuotaComponent } from './quota/quota.component';
import { QuotasComponent } from './quotas/quotas.component';
import { BalancesComponent } from './balances/balances.component';
import { PaymethodsComponent } from './paymethods/paymethods.component';
import { BalanceComponent } from './balance/balance.component';
import { FormsModule } from '@angular/forms';
import { ChangeProfileComponent } from './change-profile/change-profile.component';
import { PipesModule } from '../pipes/pipes.module';
import { InvoicesComponent } from './invoices/invoices.component';



@NgModule({
    declarations: [
        HeaderComponent,
        ApexchartComponent,
        ActivitiesComponent,
        MessagesComponent,
        NewsContainerComponent,
        EntityComponent,
        DocumentsComponent,
        WorkplacesComponent,
        ProfileChangesComponent,
        LegalTutorComponent,
        ErrorComponent,
        NotesComponent,
        QuotaComponent,
        QuotasComponent,
        BalanceComponent,
        BalancesComponent,
        PaymethodsComponent,
        ChangeProfileComponent,
        InvoicesComponent
    ],
    exports: [
        HeaderComponent,
        ApexchartComponent,
        ActivitiesComponent,
        MessagesComponent,
        NewsContainerComponent,
        EntityComponent,
        DocumentsComponent,
        WorkplacesComponent,
        ProfileChangesComponent,
        LegalTutorComponent,
        ErrorComponent,
        NotesComponent,
        QuotasComponent,
        BalancesComponent,
        PaymethodsComponent,
        ChangeProfileComponent,
        InvoicesComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        TranslateModule.forChild(),
        NgApexchartsModule,
        FormsModule,
        PipesModule
    ]
})
export class ComponentsModule { }

import { Component, Input, OnInit } from '@angular/core';
import { IonDatetime, IonModal } from '@ionic/angular';
import { format, parseISO } from 'date-fns';
import 'date-fns/locale';
import { Balance } from '../../interfaces/treasury';
import { Filter } from '../../interfaces/generic';
import { TreasuryService } from '../../services/treasury.service';
import { TranslateService } from '@ngx-translate/core';
import * as locales from 'date-fns/locale';
import { UiServiceService } from '../../services/ui-service.service';
import { ProfileService } from '../../services/profile.service';

@Component({
    selector: 'app-balances',
    templateUrl: './balances.component.html',
    styleUrls: ['./balances.component.scss'],
})
export class BalancesComponent implements OnInit {

    @Input() title: string;
    @Input() active: boolean;
    year: string = new Date().getFullYear().toString();
    month: string;
    monthShow: string;
    page: number = 1;
    hasMore: boolean = false;
    balances: Balance[];
    years: string[];
    months: string[];
    searchedText: string;

    constructor(
        private treasuryService: TreasuryService,
        private translate: TranslateService,
        private ui: UiServiceService,
        private profileService: ProfileService
    ) { }

    ngOnInit() {
        this.setData(this.page);
        this.profileService.changedProfile.subscribe(() => this.setData(1));
    }

    async setData(page: number, event?) {
        await this.getBalances(page, this.year);

        if (event) {
            event.target.complete();
        }
    }

    changeMonth(month: IonDatetime, modal: IonModal) {
        var date = month.value.match(/(\d{4}-)([01]\d-)([0-3]\d)/);

        const locale: Locale = locales[this.translate.currentLang];
        const monthConvert = date[1] + date[2] + '01';

        this.month = monthConvert;
        this.monthShow = format(parseISO(monthConvert), 'MMM', { locale: locale }).toUpperCase();
        modal.dismiss();
        this.search();
    }

    changeYear(year: string, modal: IonModal) {
        this.year = year;
        modal.dismiss();
        this.search();
    }

    search() {
        var convertMonth;
        if (this.month != null) {
            convertMonth = parseInt(format(parseISO(this.month), 'MM'));
        }
        this.getBalances(1, this.year, convertMonth, this.searchedText);
    }

    async getBalances(page: number, year: string, month?: number, text?: string) {

        const filter: Filter[] = this.filter(year, month, text);
        this.treasuryService.getBalances('partialpayments', 20, filter, '-date', page).then(resp => {
            if (resp.status == 'OK') {
                const years = Object.keys(resp.years);
                if (years.length > 0) {
                    if (years.includes(year.toString())) {
                        this.years = years;
                        this.year = year;
                        this.months = Object.values(resp.years[year]);
                        this.balances = resp.result;
                        this.hasMore = this.page < resp.lastPage;
                        this.page = ++page;
                    } else {
                        this.getBalances(this.page, Object.keys(resp.years)[0]);
                    }
                } else {
                    this.years = [];
                    this.year = year;
                    this.months = [];
                    this.balances = [];
                    this.hasMore = false;
                    this.page = page;
                }

            }
        }).catch(() => this.ui.alertInfo(this.translate.instant('errors.genericerror')));
    }

    filter(year: string, month: number, text?: string): Filter[] {
        let filter: Filter[] = [
            {
                field: 'date.year',
                value: year
            },
        ];

        if (month) {
            filter.push({ field: 'date.month', value: month })
        }

        if (text) {
            filter.push({ field: 'title', value: text })
        }
        return filter;
    }

    reset(modal: IonModal, type: string) {
        if (type == 'month') {
            this.month = null;
            this.monthShow = null;
        } else {
            this.year = this.years[0] ?? new Date().getFullYear().toString();
        }
        this.search();
        modal.dismiss();
    }

    async payBalance(balance: Balance) {
        this.ui.presentLoading(this.translate.instant('messages.saving'));
        const paid = await this.treasuryService.payBalance(balance);
        if (paid) {
            this.treasuryService.paidBalance.subscribe(resp => {
                let updateItem = this.balances.find(balance => balance.id == resp.id);
                this.balances.splice(this.balances.indexOf(updateItem), 1, resp);
            });
        }
        this.ui.closeLoading();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageSanitazerPipe } from './image-sanitazer.pipe';
import { FilterPipe } from './filter.pipe';



@NgModule({
    declarations: [
        ImageSanitazerPipe,
        FilterPipe
    ],
    exports: [
        ImageSanitazerPipe,
        FilterPipe
    ],
    imports: [
        CommonModule
    ]
})
export class PipesModule { }

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(array: any[], id: number): any[] {
        if (!array) {
            return array;
        }

        return array.filter( item => item.entity.id == id)
    }

}

import { Profile } from './users';
export interface ActivitiesResponse {
    status: string;
    result?: Activity[];
    error?: string[] | string;
    lastPage?: number;
}

export interface ActivityResponse {
    status: string;
    result?: Activity;
    error?: string[] | string;
}

export interface AttendanceReponse {
    status: string;
    result?: Attendance[];
    error?: string[] | string;
    lastPage?: number;
}

export interface Activity {
    id?:                 number;
    country?:            string;
    title?:              string;
    img?:                string;
    phone?:              string;
    address?:            string;
    pc?:                 number;
    town?:               string;
    province?:           string;
    dateStart?:          string;
    dateEnd?:            string;
    capacity?:           number;
    description?:        string;
    type?:               Type;
    profile?:            ActivityProfile;
    affecteds?:          ActivityProfile[];
    waiting?:            boolean;
    schedules?:          Schedule[];
    canEnroll?:          boolean;
}

export interface Type {
    id?:         number;
    entity_id?:  number;
    title?:      string;
}

export interface ActivityProfile {
    id?:         number;
    activity?:   Activity;
    profile_id?: number;
    profile?:    Profile;
    registered?: number;
}

export interface Attendance {
    activity?:          Activity;
    attendanceProfile?: AttendanceProfile;
    date?:              Date;
    hour?:              string;
}

export interface AttendanceProfile {
    attendance?:    Attendance;
    profile?:       Profile;
    status?:        number;
    statusImg?:     string;
}

export interface Schedule {
    id?:        number;
    dayOfWeek?: number;
    timeStart?: string;
    timeEnd?:   string;
    title?:     string;
}
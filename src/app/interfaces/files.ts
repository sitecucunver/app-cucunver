import { Profile } from './users';

export interface FilesResponse {
    status: string;
    result?: CucunverFile[];
    error?: string[] | string;
    lastPage?: number;
    permit?: boolean;
}

export interface FileResponse {
    status: string;
    result?: CucunverFile;
    error?: string[] | string;
}

export interface CucunverFile {
    id?: number;
    creator_id?: number;
    parent_id?: number;
    affected_id?: number;
    name: string;
    section: string;
    type: number;
    img_type?: string;
    filePath?: string;
    fileName?: string;
    parent?: CucunverFile;
    creator?: Profile;
    updated_at?: Date;
    file?: any;
}

import { ChooserResult } from '@awesome-cordova-plugins/chooser/ngx';
import { Profile } from './users';
export interface MessagesResponse {
    status: string;
    result?: Message[];
    error?: string[] | string;
    lastPage?: number;
}

export interface MessageResponse {
    status: string;
    result?: Message;
    error?: string[] | string;
}

export interface Message {
    id?:                  number;
    parent_id?:           number;
    parent?:              Message;
    entity_id?:           number;
    title?:               string;
    txt?:                 string;
    filePath?:            string,
    fileName?:            string,
    fileImg?:             string,
    fileFlag?:            string,
    affectedContest?:     number;
    affectedAttach?:      number;
    comunicationDateEnd?: number;
    dateEnd?:             null;
    wasEditor?:           number;
    closed?:              number;
    sender?:              Profile;
    editor?:              Profile;
    receivers?:           Receiver[];
    receiver?:            Receiver;
    created_at?:          string;
    iAmOwner?:            boolean;
    file?:                ChooserResult;
    read?:                boolean;
    lastUpdate?:          number;
}

export interface Receiver {
    id?:         number;
    profile_id?: number;
    visible?:    number;
    status?:     number;
    read?:       number;
    isEditor?:   number;
    isAffected?: number | null;
    removed?:    number;
}

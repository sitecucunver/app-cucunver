export interface NewsResponse {
    status: string;
    result?: News[];
    error?: string[] | string;
    lastPage?: number;
}

export interface NewsByIdResponse {
    status: string;
    result?: News;
    error?: string[] | string;
}

export interface News {
    id:          number;
    title:       string;
    link:        string;
    img:         string;
    description: string;
    date:        string;
    tags:        NewsTag[];
    entity_id:   number;
}

export interface NewsTag {
    id:     number;
    tag:    Tag;
}

export interface Tag {
    id:         string;
    entity_id:  number;
    title:      string;
    color:      string;
    section:    string;
}

import { Country, Province } from './generic';

export interface UsersResponse {
    status: string;
    result?: User[];
    error?: string[] | string;
}

export interface UserResponse {
    status: string;
    result?: User;
    error?: string[] | string;
}

export interface ProfilesResponse {
    status: string;
    result?: Profile[];
    error?: string[] | string;
}

export interface ProfileResponse {
    status: string;
    result?: Profile;
    error?: string[] | string;
}

export interface WorkplacesResponse {
    status: string;
    result?: Workplace[];
    error?: string[] | string;
}
export interface LegalTutorsResponse {
    status: string;
    result?: LegalTutor[];
    error?: string[] | string;
}

export interface LegalTutorResponse {
    status: string;
    result?: LegalTutor;
    error?: string[] | string;
}

export interface AffiliateCardResponse {
    status: string;
    result?: AffiliateCard;
    error?: string[] | string;
}

export interface User {
    id: number;
    role_id: number;
    entity_id: number;
    profile_id: number;
    country: number;
    language: string;
    timeZone: string;
    currency: number;
    disabled?: number;
    dateDeleted?: null;
    email: string;
    firstName: string;
    lastName: string;
    avatar: string;
    phone: string;
    phone2: null;
    identification_type: number;
    nif: string;
    pc: string;
    town: string;
    province: string;
    address: null;
    shortName?: string;
    category?: number;
    registryNumber?: string;
    registrationDate?: string;
    businessName?: string;
    socialAddress?: string;
    sameAddress?: number;
    fiscalAddress?: string;
    fiscalPc?: string;
    fiscalTown?: string;
    fiscalProvince?: string;
    fiscalCountry?: number;
    website?: string;
    currentProfile?: Profile;
    typePayment?: number;
    defaultPayment_id?: number;
    birthday?: null;
    gender?: null;
    currentEntity?: User;
    profiles?: Profile[];
    socialNetworks?: SocialNetwork[];
    showWorkplace: boolean;
    showNShops: boolean;
    showSpecialityEESTO: boolean;
    showIsNurse: boolean;
    required_allfields: boolean;
    only_card: boolean;
    showBreeder: boolean;
    options: Options;
    hasPlan: boolean;
}

export interface Profile {
    id?: number;
    typePayment?: number;
    defaultPayment_id?: number;
    affiliate_id?: number;
    hasPetition?: number;
    isEditor?: number;
    editor_at?: Date;
    isAffiliate?: number;
    disabled?: number;
    removed?: number;
    firstName: string;
    firstName_tmp?: string;
    email?: string;
    email_tmp?: string;
    avatar?: string;
    avatar_tmp?: any;
    phone?: string;
    phone_tmp?: string;
    phone2?: string;
    phone2_tmp?: string;
    identification_type?: number;
    identification_type_tmp?: number;
    nif?: string;
    nif_tmp?: string;
    pc?: string;
    pc_tmp?: string;
    town?: string;
    town_tmp?: string;
    province?: string;
    province_tmp?: string;
    country?: Country;
    country_tmp?: Country;
    affiliateNumber?: number;
    affiliateDate?: string;
    address?: string;
    address_tmp?: string;
    lastName?: string;
    lastName_tmp?: string;
    birthday?: string;
    birthday_tmp?: string;
    gender?: number;
    gender_tmp?: number;
    addressed?: string;
    addressed_tmp?: string;
    nacionality?: string;
    nacionality_tmp?: string;
    charge?: null;
    charge_tmp?: null;
    unit?: null;
    unit_tmp?: null;
    speciality?: null;
    speciality_tmp?: null;
    agreement?: number;
    agreement_tmp?: number;
    n_shops?: null;
    n_shops_tmp?: null;
    affiliate?: Affiliate;
    user?: User;
    legalTutors?: LegalTutor[];
    dependents?: LegalTutor[];
    workplace?: Workplace;
    workplace_tmp?: Workplace;
    payment?: Payment;
    payments?: Payment[];
    fullName?: string;
    is_nurse?: boolean;
    is_nurse_tmp?: boolean;
    breeder?: string;
    breeder_tmp?: string;
    entity?: User;
    entity_id?: number;
    role_id?: number;
    nifError?: string | string[];
    showAffiliateCard?: boolean;
    showCreditCard?: boolean;
}

export interface Affiliate {
    id: number;
    name: string;
}

export interface Payment {
    id: number;
    type: number;
    title: string;
    number: string;
    caducity: string;
    holder: string;
}

export interface SocialNetwork {
    id: number;
    url: string;
    icon: string;
}

export interface Workplace {
    id: number,
    name: string
    address: string,
    pc: string,
    town: string,
    province: Province | string,
    country: Country,
}

export interface LegalTutor {
    id?: number;
    tutor?: Profile;
    younger?: Profile;
    relation: string;
    supervisor?: number;
    supervisorIcon?: Array<any>|string;
}

export interface Options {
    id: number;
    silent_app: boolean;
    activity_registry: boolean;
    email_notifications: boolean;
    me_data_request: boolean;
    me_activities: boolean;
    me_comunications_reception: boolean;
}

export interface AffiliateCard {
    affiliate?: Affiliate;
    affiliate_id?: number;
    color?: string;
    title?: string;
    validateStart?: string;
    validateEnd?: string;
    qr?: string
}
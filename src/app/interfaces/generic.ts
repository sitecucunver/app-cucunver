import { ChooserResult } from "@awesome-cordova-plugins/chooser/ngx";
import { Profile } from "./users";

export interface MenuOpt {
    icon: string;
    name: string;
    redirectTo: string;

}

export interface Timezone {
    value: string;
    tag: string;
}

export interface Lang {
    value: string;
    tag: string;
}

export interface LoginResponse {
    user_id?:      number,
    token_type:    string;
    expires_in:    number;
    access_token:  string;
    refresh_token: string;
    date_expire?:  Date;
}

export interface CountryResponse {
    status: string;
    result?: Country[];
    error?: string[] | string;
}

export interface NotificationsResponse {
    status: string;
    result?: Notification[];
    error?: string[] | string;
    lastPage?: number;
}

export interface Country {
    id:        number;
    name:      string;
    shortname: string;
    working:   number;
    provinces?: Province[];
}

export interface Province {
    id:             number;
    country_id:     number;
    name:           string;
    code:           string;
    community_code: string;
}

export interface Filter {
    field: string;
    value: any
}

export interface GenericResponse {
    status: string;
    error?: string[] | string;
}

export interface NotesResponse {
    status: string;
    result: Note[];
    error?: string[] | string;
    lastPage?: number;
}

export interface NoteResponse {
    status: string;
    result: Note;
    error?: string[] | string;
}

export interface Note {
    id?: number;
    text?: string,
    filePath?: string,
    fileName?: string,
    fileImg?: string,
    fileFlag?:string,
    profile?: Profile,
    editor?: Profile,
    file?: ChooserResult,
    table_id?: number,
    tableName?: string,
    updated_at?: Date;
}
import { Profile } from './users';
export interface BalancesGraficReponse {
    status: string;
    result?: BalanceGrafic;
    error?: string[] | string;
}
export interface QuotasResponse {
    status: string;
    result?: Quota[];
    error?: string[] | string;
}

export interface ProfileQuotasResponse {
    status: string;
    result?: ProfileQuota[];
    error?: string[] | string;
}

export interface ProfileQuotaYearsResponse {
    status: string;
    result?: ProfileQuotaYear[];
    error?: string[] | string;
    years?: any[];
    lastPage?: number;
}
export interface BalancesResponse {
    status: string;
    result?: Balance[];
    error?: string[] | string;
    years?: any[];
    lastPage?: number;
}

export interface BalanceResponse {
    status: string;
    result?: Balance;
    error?: string[] | string;
}

export interface PaymethodsResponse {
    status: string;
    result?: Paymethod[];
    error?: string[] | string;
    lastPage?: number;
}
export interface PaymethodResponse {
    status: string;
    result?: Paymethod;
    error?: string[] | string;
}

export interface InvoicesResponse {
    status: string;
    result?: Invoice[];
    error?: string[] | string;
    lastPage?: number;
}

export interface ProfileQuota {
    id?: number;
    periodicity?: number;
    amount?: number;
    fees?: number;
    dateStart?: string;
    dateEnd?: string;
    typePayment?: number;
    paymethod?: Paymethod;
    customized?: number;
    disabled?: number;
    pqy?: ProfileQuotaYear[];
    quota?: Quota;
}

export interface Paymethod {
    id?: number;
    type?: number;
    title?: string;
    numberFormat?: string;
    number?: string;
    caducity?: string;
    holder?: string;
    default?: string;
    created_at?: Date;
    cvv?: number;
    profile_id?: number;
    profile?: Profile;
}

export interface Quota {
    id?: number;
    title?: string;
    periodicity?: number;
    dateStart?: string;
    dateEnd?: string;
    amount?: number;
    fees?: null;
    disabled?: number;
}

export interface ProfileQuotaYear {
    id?: string;
    quota?: Quota;
    quotaYear?: QuotaYear;
    year?: string;
    pq?: ProfileQuota;
    month1_amount?: number;
    month1_paid?: number;
    month1_paidImg?: string;
    month1_paymenttype?: number;
    month1_disabled?: boolean;
    month2_amount?: number;
    month2_paid?: number;
    month2_paidImg?: string;
    month2_paymenttype?: number;
    month2_disabled?: boolean;
    month3_amount?: number;
    month3_paid?: number;
    month3_paidImg?: string;
    month3_paymenttype?: number;
    month3_disabled?: boolean;
    month4_amount?: number;
    month4_paid?: number;
    month4_paidImg?: string;
    month4_paymenttype?: number;
    month4_disabled?: boolean;
    month5_amount?: number;
    month5_paid?: number;
    month5_paidImg?: string;
    month5_paymenttype?: number;
    month5_disabled?: boolean;
    month6_amount?: number;
    month6_paid?: number;
    month6_paidImg?: string;
    month6_paymenttype?: number;
    month6_disabled?: boolean;
    month7_amount?: number;
    month7_paid?: number;
    month7_paidImg?: string;
    month7_paymenttype?: number;
    month7_disabled?: boolean;
    month8_amount?: number;
    month8_paid?: number;
    month8_paidImg?: string;
    month8_paymenttype?: number;
    month8_disabled?: boolean;
    month9_amount?: number;
    month9_paid?: number;
    month9_paidImg?: string;
    month9_paymenttype?: number;
    month9_disabled?: boolean;
    month10_amount?: number;
    month10_paid?: number;
    month10_paidImg?: string;
    month10_paymenttype?: number;
    month10_disabled?: boolean;
    month11_amount?: number;
    month11_paid?: number;
    month11_paidImg?: string;
    month11_paymenttype?: number;
    month11_disabled?: boolean;
    month12_amount?: number;
    month12_paid?: number;
    month12_paidImg?: string;
    month12_paymenttype?: number;
    month12_disabled?: boolean;
}

export interface QuotaYear {
    id?: number;
    quota?: Quota;
    year?: string;
    month1?: number;
    disabled1?: boolean;
    month2?: number;
    disabled2?: boolean;
    month3?: number;
    disabled3?: boolean;
    month4?: number;
    disabled4?: boolean;
    month5?: number;
    disabled5?: boolean;
    month6?: number;
    disabled6?: boolean;
    month7?: number;
    disabled7?: boolean;
    month8?: number;
    disabled8?: boolean;
    month9?: number;
    disabled9?: boolean;
    month10?: number;
    disabled10?: boolean;
    month11?: number;
    disabled11?: boolean;
    month12?: number;
    disabled12?: boolean;
}


export interface BalanceGrafic {
    incomes:  IncomeGrafic[];
    expenses: ExpenseGrafic[];
}

export interface ExpenseGrafic {
    x: string;
    y: number | string;
}

export interface IncomeGrafic {
    x: string;
    y: number | string;
}
export interface Balance {
    id?:                    number;
    entity_id?:             number;
    title?:                 string;
    amount?:                number;
    date?:                  string;
    fractional?:            number;
    parent_id?:             number;
    type?:                  number;
    from?:                  number;
    from_id?:               number;
    category_id?:           number;
    subcategory_id?:        number;
    box_id?:                number;
    affected_id?:           number;
    affected_text?:         string;
    status?:                number;
    statusImg?:             string;
    quota_id?:              number;
    quotaTitle?:            string;
    pqy_id?:                number;
    pqy_year?:              number;
    pqy_month?:             number;
    remittance_id?:         number;
    remittance_receipt_id?: number;
    remittance_concept_id?: number;
    activity_id?:           number;
    activityTitle?:         string;
    error?:                 number;
    disabled?:              number;
    stripe_payment_id?:     string;
    partial:                Balance[];
}

export interface Invoice {
    status?:    number;
    statusImg?: string;
    number?:    number;
    amount?:    number;
    date?:      Date;
    serial?:    string;
    billUrl:    string;
}
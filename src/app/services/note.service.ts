import { Injectable, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HelperService } from './helper.service';
import { NotesResponse, GenericResponse, Note, NoteResponse, Filter } from '../interfaces/generic';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class NoteService {

    @Output() noteUpdate = new EventEmitter<Note>();

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private uiService: UiServiceService,
        private translate: TranslateService
    ) { }

    async getNotes(section: string, id: number, included?: string, filter?: Filter[], perPage?: number, sort?: string, page?: number): Promise<NotesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<NotesResponse>(`${URL}${ENDPOINT}/note/index/${section}/${id}${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async deleteNote(id: number): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise(resolve => {
            this.http.delete<GenericResponse>(`${URL}${ENDPOINT}/note/delete/${id}`,{headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.uiService.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.uiService.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async storeNote(note: FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/note/store`, note, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.uiService.alertInfo(resp.error)
                        resolve(false);
                    }
                },
                error: () => {
                    this.uiService.alertInfo(this.translate.instant('errors.genericerror'))
                    resolve(false);
                }
            });
        });
    }

    async deleteFile(id: number): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.delete<NoteResponse>(`${URL}${ENDPOINT}/note/delete/file/${id}`, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.uiService.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.uiService.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async updateNote(note: FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<NoteResponse>(`${URL}${ENDPOINT}/note/update`, note, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.noteUpdate.emit(resp.result);
                        resolve(true);
                    } else {
                        this.uiService.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.uiService.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HelperService } from './helper.service';
import { MessagesResponse, Message, MessageResponse } from '../interfaces/messages';
import { Filter, GenericResponse } from '../interfaces/generic';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class MessagesService {

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    async getMessages(perPage?: number, included?: string, filter?: Filter[], sort?: string, page?: number): Promise<MessagesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<MessagesResponse>(`${URL}${ENDPOINT}/message/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async getMessage(id: number, included?: string): Promise<MessageResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);
        return new Promise((resolve, reject) => {
            this.http.get<MessageResponse>(`${URL}${ENDPOINT}/message/show/${id}${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async toogleRead(message: Message): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/message/toogle/read`, message, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        resolve(false);
                        this.ui.alertInfo(resp.error);
                    }
                },
                error: () => {
                    resolve(false);
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                }
            });
        });
    }

    async toogleArchive(message: Message): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/message/toogle/archive`, message, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        resolve(false);
                        this.ui.alertInfo(resp.error);
                    }
                },
                error: () => {
                    resolve(false);
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                }
            });
        });
    }

    async getMessageThread(message: Message, perPage?: number, included?: string, filter?: Filter[], sort?: string, page?: number): Promise<MessagesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);

        return new Promise((resolve, reject) => {
            this.http.get<MessagesResponse>(`${URL}${ENDPOINT}/message/thread/${message.id}${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async store(message: FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/message/store`, message, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () =>{
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async storeChild(message: FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/message/store/child`, message, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async update(message: FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/message/update`, message, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async deleteFile(id: number): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.delete<GenericResponse>(`${URL}${ENDPOINT}/message/delete/file/${id}`, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }
}

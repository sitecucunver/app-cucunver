import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HelperService } from './helper.service';
import { FilesResponse, CucunverFile, FileResponse } from '../interfaces/files';
import { Filter, GenericResponse } from '../interfaces/generic';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class DocumentsService {

    @Output() fileUpdate = new EventEmitter<CucunverFile>();

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    async getDocuments(section: string, id?: number, included?: string, perPage?: number, filter?: Filter[], sort?: string, page?: number): Promise<FilesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        var route: string;
        if (id != null) {
            route = `${URL}${ENDPOINT}/file/index/${section}/${id}${parameters}`;
        } else {
            route = `${URL}${ENDPOINT}/file/index/${section}${parameters}`
        }

        return new Promise((resolve, reject) => {
            this.http.get<FilesResponse>(route, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async storeFile(cucunverFile: CucunverFile | FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/file/store`, cucunverFile, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error)
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async updateFile(cucunverFile: CucunverFile): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<FileResponse>(`${URL}${ENDPOINT}/file/update`, cucunverFile, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.fileUpdate.emit(resp.result);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error)
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async deleteFile(cucunverFile: CucunverFile): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.delete<GenericResponse>(`${URL}${ENDPOINT}/file/delete/${cucunverFile.id}`, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error)
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }
}

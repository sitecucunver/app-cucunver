import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class UiServiceService {

    constructor(
        private alertController: AlertController,
        private translate: TranslateService,
        private loadingController: LoadingController,
        private toastController: ToastController
    ) { }

    async alertInfo(msg: Object | Array<string> | string) {
        let message = '';
        if (typeof msg === 'object') {
            for (let error of Object.values(msg)) {
                message += error + '</br>';
            }
        } else if (Array.isArray(msg)) {
            message = msg[0];
        } else {
            message = msg;
        }
        const alert = await this.alertController.create({
            message,
            buttons: ['OK']
        });

        await alert.present();
    }

    async confirmUi(title: string, message: string) {
        const alert = await this.alertController.create({
            header: title,
            message: message,
            buttons: [
                {
                    text: this.translate.instant('messages.no'),
                    role: 'cancel',
                    cssClass: 'secondary',
                },
                {
                    text: this.translate.instant('messages.yes'),
                    role: 'confirm'
                },
            ],
        });

        await alert.present();

        const { role } = await alert.onDidDismiss();
        return role;

    }

    async presentLoading(message: string) {
        const loading = await this.loadingController.create({
            message,
        });
        await loading.present();
    }

    closeLoading() {
        this.loadingController.dismiss();
    }

    async toastMessage(message: string) {
        const toast = await this.toastController.create({
            message: message,
        });

        await toast.present();
    }
}

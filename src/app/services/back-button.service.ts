import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavController } from '@ionic/angular';
@Injectable({
    providedIn: 'root'
})
export class BackButtonService {

    constructor(
        private platform: Platform,
        private router: Router,
        private navController: NavController,
    ) { }

    init() {
        this.platform.backButton.subscribe(() => {
            const currentUrl = this.router.url;
            if (currentUrl === "/home" || currentUrl === "/login") {
                navigator['app'].exitApp();
            } else {
                this.navController.back();
            }
        });
    }
}

import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HelperService } from './helper.service';
import { ProfileResponse, WorkplacesResponse, Profile, LegalTutorsResponse, ProfilesResponse } from '../interfaces/users';
import { CountryResponse, Filter, GenericResponse } from '../interfaces/generic';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    @Output() canceledPetition = new EventEmitter<string>();
    @Output() newLegalTutor = new EventEmitter<boolean>();
    @Output() changedProfile = new EventEmitter<boolean>();

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    async getProfile(id: number, included?: string): Promise<ProfileResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);

        return new Promise((resolve, reject) => {
            this.http.get<ProfileResponse>(`${URL}${ENDPOINT}/profile/show/${id}${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async getProfiles(filter?: Filter[], included?: string, sort?: string, perPage?: number, page?: number): Promise<ProfilesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);

        return new Promise((resolve, reject) => {
            this.http.get<ProfilesResponse>(`${URL}${ENDPOINT}/profile/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async getCountries(): Promise<CountryResponse> {
        const headers = await this.helperService.getHeaders();

        return new Promise((resolve, reject) => {
            this.http.get<CountryResponse>(`${URL}${ENDPOINT}/config/countries`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject()
            });
        });
    }

    async getWorkplace(filter?: Filter[], included?: string, sort?: string, perPage?: number, page?: number) {
        const headers = await this.helperService.getHeaders();

        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);

        return this.http.get<WorkplacesResponse>(`${URL}${ENDPOINT}/profile/workplace/index${parameters}`, { headers });
    }

    async updateProfile(profile: Profile | FormData): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/profile/update`, profile, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async cancelPetition(): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/profile/cancelPetition`, [], { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.canceledPetition.emit(resp.status);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false)
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async getLegalTutors(included?: string, filter?: Filter[], perPage?: number, sort?: string, page?: number) {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return this.http.get<LegalTutorsResponse>(`${URL}${ENDPOINT}/legalTutor/index${parameters}`, { headers });
    }

    async legalTutorStore(legalTutor: any): Promise<boolean> {
        const headers = await this.helperService.getHeaders();

        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/legalTutor/store`, legalTutor, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.newLegalTutor.emit(true);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async changeProfile(profile_id: number): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        await this.ui.presentLoading(this.translate.instant('messages.changingProfile'));
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/user/change/profile`, { profile_id }, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.changedProfile.emit(true);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error)
                        resolve(false);
                    }
                    this.ui.closeLoading();
                }, error: () => {
                    resolve(false);
                    this.ui.closeLoading();
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                }
            })
        });
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserResponse, AffiliateCardResponse } from '../interfaces/users';
import { environment } from '../../environments/environment';
import { HelperService } from './helper.service';


const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class EntityService {

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
    ) { }

    async getEntity(included?: string): Promise<UserResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);
        return new Promise((resolve, reject) => {
            this.http.get<UserResponse>(`${URL}${ENDPOINT}/entity/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error:  () => reject()
            });
        });

    }

    async getAffiliateCard(included?: string): Promise<AffiliateCardResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);
        return new Promise((resolve, reject) => {
            this.http.get<AffiliateCardResponse>(`${URL}${ENDPOINT}/entity/affiliatecard/user${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error:  () => reject()
            });
        });
    }
}

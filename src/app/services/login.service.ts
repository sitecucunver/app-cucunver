import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { LoginResponse, GenericResponse } from '../interfaces/generic';
import { User, UserResponse } from '../interfaces/users';
import { TranslateService } from '@ngx-translate/core';
import { PushService } from './push.service';

const URL = environment.url;
const ENDPOINT = environment.endpoint;
const CLIENT_ID = environment.client_id;
const CLIENT_SECRET = environment.client_secret;

const headers = new HttpHeaders({
    'Accept': 'application/json',
});

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private loginToken: LoginResponse;
    private user: User;
    @Output() logued = new EventEmitter<boolean>();

    constructor(
        private http: HttpClient,
        private storage: Storage,
        private navController: NavController,
        private translate: TranslateService,
        private push: PushService
    ) {
        this.init();
    }

    async init() {
        await this.storage.create();
    }

    /**
     *  Se ejecuta solo al loguear y se hace una llamda al endpoint login de cucunver
     *
     * @param email string
     * @param password string
     * @returns Promise<boolean>
     */
    async login(email: string, password: string): Promise<boolean> {
        const loginParameters = {
            'email': email,
            'password': password
        };

        return new Promise((resolve) => {
            this.http.post<UserResponse>(`${URL}${ENDPOINT}/login`, loginParameters, { headers }).subscribe({
                next: async (userResponse) => {
                    if (userResponse.status == 'OK') {
                        const valid = await this.getToken(loginParameters, userResponse.result);
                        this.logued.emit(true);
                        resolve(valid);
                    } else {
                        this.removeLogin();
                        resolve(false);
                    }
                },
                error: () => {
                    this.removeLogin();
                    resolve(false);
                }
            });
        });
    }


    /**
     * Se ejecuta cuando el loguin fue existoso y se llama a un endpoint para obtener el token del usuario
     * @param login Object
     * @param user User
     * @returns Promise<boolean>
     */
    async getToken(login, user: User): Promise<boolean> {

        const data = {
            grant_type: "password",
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            username: login.email,
            password: login.password
        };

        return new Promise(resolve => {
            this.http.post<LoginResponse>(`${URL}/oauth/token`, data, { headers }).subscribe({
                next: async resp => {
                    await this.storeToken(resp, user);
                    const headers = new HttpHeaders({
                        'Accept': 'application/json',
                        'Authorization': `Bearer ${this.loginToken.access_token}`,
                    });
                    const status = await this.push.storeDevice(headers);
                    resolve(status);
                },
                error: () => {
                    this.removeLogin();
                    resolve(false);
                }
            });
        });
    }

    /**
     * Se utiliza para guardar en el storage el usuario y el token
     * @param respuesta LoginRepsonse
     * @param user User
     */
    async storeToken(respuesta: LoginResponse, user: User) {
        //Guardar usuario
        this.user = user;
        await this.storage.set('user', user);
        //Guardar token
        this.loginToken = respuesta;
        this.loginToken.user_id = user.id;
        const date = new Date();
        date.setTime(date.getTime() + respuesta.expires_in * 1000)
        this.loginToken.date_expire = date;

        await this.storage.set('token', respuesta);
    }

    /**
     * Se utiliza para eliminar del storage el usuario y el token
     */
    removeLogin() {
        //Se limpian las variables y el storage
        this.user = null;
        this.loginToken = null;
        this.storage.remove('user');
        this.storage.remove('token');
    }

    /**
     * Confirma que el token y el usuario existen y el token no haya caducado
     * @returns Promise<boolean>
     */
    async validateToken(): Promise<boolean> {
        await this.setUserAndToken();

        return new Promise(async (resolve) => {
            if (this.user == null || this.loginToken == null) {
                this.navController.navigateRoot('/login');
                resolve(false);
            } else {
                const curdate = new Date();
                if (this.translate.currentLang == null) {
                    this.translate.use(this.user.language);
                }

                if (curdate > this.loginToken.date_expire) {
                    const status = await this.refreshToken();
                    resolve(status);
                } else {
                    resolve(true);
                }

                if (this.user.profile_id == null) {
                    this.navController.navigateRoot('/noentity');
                } else if (!this.user.currentEntity.hasPlan) {
                    this.navController.navigateRoot('/entity-no-plan');
                } else if (this.user.role_id == 3 || this.user.role_id == 4 || this.user.currentProfile.isEditor) {
                    this.navController.navigateRoot('/under-contruction');
                }

            }
        });
    }

    /**
     * Se utiliza para cargar en las variabes el token y el usuario
     */
    async setUserAndToken() {
        this.user = await this.storage.get('user') || null;
        this.loginToken = await this.storage.get('token') || null;
    }

    /**
     * Refresca el token a partir del refresh_token
     * @returns Promise<boolean>
     */
    async refreshToken(): Promise<boolean> {
        const data = {
            grant_type: "refresh_token",
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            refresh_token: this.loginToken.refresh_token
        };
        return new Promise(resolve => {
            this.http.post<LoginResponse>(`${URL}/oauth/token`, data, { headers }).subscribe({
                next: async resp => {
                    await this.storeToken(resp, this.user)
                    resolve(true);
                },
                error: () => {
                    this.removeLogin();
                    resolve(false);
                }
            });
        });
    }

    logout(route: string = '/login') {
        const headers = new HttpHeaders({
            'Accept': 'application/json',
            'Authorization': `Bearer ${this.loginToken.access_token}`,
        });
        this.push.deleteDevice(headers);
        this.removeLogin();
        this.navController.navigateRoot(route, { animated: true });
    }

    async getAccessToken() {
        const valid = await this.validateToken();

        if (valid) {
            return this.loginToken.access_token;
        } else {
            return null;
        }
    }

    async setUser(user: User) {
        this.user = user;
        await this.storage.set('user', user);
    }

    async getUser() {
        if (this.user == null) {
            await this.setUserAndToken();
        }
        return this.user;
    }

    forgotten(email: string) {
        const forgottenParameters = {
            'email': email,
        };

        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/forgotten`, forgottenParameters, { headers }).subscribe({
                next: async (userResponse) => {
                    if (userResponse.status == 'OK') {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                },
                error: () => {
                    resolve(false);
                }
            });
        });
    }
}

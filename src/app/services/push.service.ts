import { EventEmitter, Injectable, Output } from '@angular/core';
import { OneSignal, OSNotificationOpenedResult } from '@awesome-cordova-plugins/onesignal/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericResponse } from '../interfaces/generic';
import { environment } from 'src/environments/environment';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

const URL = environment.url;
const ENDPOINT = environment.endpoint;
const APPID = environment.onesignal_app_id;
const FIREBASEID = environment.firebase_id;

@Injectable({
    providedIn: 'root'
})
export class PushService {

    deviceId: string;
    @Output() activity = new EventEmitter<number>();

    constructor(
        private oneSignal: OneSignal,
        private http: HttpClient,
        private navController: NavController
    ) { }

    init() {
        this.oneSignal.startInit(APPID, FIREBASEID);
        //Obtener id del subscriptor
        this.oneSignal.getIds().then(info => {
            this.deviceId = info.userId;
        });

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationOpened().subscribe((noti: OSNotificationOpenedResult) => {
            const data = noti.notification.payload.additionalData;
            this.redirectToSection(data.section, data.action, data.id, data.profile_id);
        });

        this.oneSignal.endInit();
    }


    async storeDevice(headers: HttpHeaders): Promise<boolean> {
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/user/device/store`, { device: this.deviceId }, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                },
                error: () => resolve(false)
            });
        })
    }

    async deleteDevice(headers: HttpHeaders): Promise<boolean> {
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/user/device/delete`, { device: this.deviceId }, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                },
                error: () => resolve(false)
            });
        })
    }

    async redirectToSection(section: number, action: number, id: number, profile_id?: number) {
        let navigationExtras: NavigationExtras = {};
        switch (section) {
            case 0: //Notificaciones globales
                navigationExtras.queryParams = { section, action, profile_id };
                this.navController.navigateForward('/myprofile/profiledata', navigationExtras);
                break;
            case 8: //Solicitud de cambios de datos de mi perfil
                navigationExtras.queryParams = { section, action, profile_id };
                this.navController.navigateForward('/myprofile/profiledata', navigationExtras);
                break;
            case 9: //Invitaciones
                navigationExtras.queryParams = { section, action, id, profile_id };
                this.navController.navigateForward('/myprofile/profiledata', navigationExtras);
                break;
            case 10: //Actividades
                navigationExtras.queryParams = { profile_id };
                this.navController.navigateForward(`/activity/${id}`, navigationExtras);
                break;
            case 11: //Recepción de comunicaciones
                navigationExtras.queryParams = { id, profile_id };
                this.navController.navigateForward('/messages', navigationExtras);
                break;
            case 16: //Noticias
                navigationExtras.queryParams = { id, profile_id };
                this.navController.navigateForward('/news', navigationExtras);
                break;
            case 17: //Stripe
                navigationExtras.queryParams = { section, action, profile_id };
                this.navController.navigateForward('/myprofile/treasury', navigationExtras);
                break;
            default:
                break;
        }
    }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HelperService } from './helper.service';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { GenericResponse } from '../interfaces/generic';
import { User, Options } from '../interfaces/users';


const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class ConfigurationService {

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }


    async changePassword(changePasswordObj: any): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/config/change/password`, changePasswordObj, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            })
        });
    }

    async changeEmail(changeEmailObj: any): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/config/change/email`, changeEmailObj, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            })
        });
    }

    async changeTimezoneAndLang(user: User): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/config/change/timezone`, user, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            })
        });
    }

    async deleteAccount(deleteAccountObj) {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/config/delete/account`, deleteAccountObj, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            })
        });
    }

    async changeNotifications(options: Options): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/config/update/options`, options, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            })
        });
    }

}

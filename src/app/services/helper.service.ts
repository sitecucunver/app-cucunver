import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApexLocale } from 'ng-apexcharts';
import { Filter, MenuOpt, Timezone, Lang } from '../interfaces/generic';
import { LoginService } from './login.service';
import { User, UserResponse } from '../interfaces/users';
import { environment } from 'src/environments/environment';
import { UiServiceService } from './ui-service.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

const TINYMCE_API_KEY = environment.tinymce_api_key;
const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    constructor(
        private http: HttpClient,
        private loginService: LoginService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) {
    }


    getMenuOpts() {
        return this.http.get<MenuOpt[]>('/assets/data/menu-opts.json');
    }

    getTimezones() {
        return this.http.get<Timezone[]>('/assets/data/timezones.json');
    }

    getLanguages() {
        return this.http.get<Lang[]>('/assets/data/langs.json');
    }

    createRouteParams(included?: string, perPage?: number, filters?: Filter[], sort?: string, page?: number): string {
        var parameters = '';

        if (included) {
            parameters = `?included=${included}`;
        }

        if (perPage) {
            parameters += (parameters != '') ? `&perPage=${perPage}` : `?perPage=${perPage}`;
        }

        if (filters) {
            filters.forEach(filter => {
                if (filter.value != null) {
                    parameters += (parameters != '') ? `&filter[${filter.field}]=${filter.value}` : `?filter[${filter.field}]=${filter.value}`;
                }
            });
        }

        if (sort) {
            parameters += (parameters != '') ? `&sort=${sort}` : `?sort=${sort}`;
        }

        if (page) {
            parameters += (parameters != '') ? `&page=${page}` : `?page=${page}`;
        }

        return parameters;
    }

    async getHeaders(): Promise<HttpHeaders> {
        const accessToken = await this.loginService.getAccessToken();

        if (accessToken == null) {
            return null;
        } else {
            return new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            });
        }

    }

    async getUserAndSave(): Promise<User> {
        const headers = await this.getHeaders();
        if (headers != null) {
            const included = 'currentProfile.legalTutors,currentProfile.dependents,currentProfile.workplace,currentProfile.workplaceTmp,currentProfile.payments,currentProfile.affiliate,currentEntity,options,profiles.entity';
            const parameters = this.createRouteParams(included);
            return new Promise(resolve => {
                this.http.get<UserResponse>(`${URL}${ENDPOINT}/user/current${parameters}`, { headers }).subscribe({
                    next: resp => {
                        if (resp.status == 'OK') {
                            this.loginService.setUser(resp.result);
                            resolve(resp.result);
                        } else {
                            this.ui.alertInfo(resp.error);
                            resolve(null);
                        }
                    },
                    error: () => {
                        this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                        resolve(null);
                    }
                });
            });
        }
    }

    getUser() {
        return this.loginService.getUser();
    }

    getApexChartLocale(lang: string): Observable<ApexLocale> {
        return this.http.get<ApexLocale>(`assets/apexcharts/${lang}.json`);
    }

    checkNIF(type: number, nif: string) {
        let valid = false, number, letters, pattern, result, control: any;
        nif = nif.toUpperCase();
        switch (type) {
            case 1:
                pattern = /([0-9]{8})([A-Za-z])/;
                result = nif.match(pattern);
                if (result) {
                    number = result[1] % 23;
                    letters = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
                    if (result[2] == letters[number]) {
                        valid = true;
                    }
                }
                break;
            case 2:
                pattern = /^([ABCDEFGHJKLMNPQRSUVW])([0-9]{7})([0-9A-J])$/;
                let cif, tmp, sum, letter;
                result = nif.match(pattern);
                if (result) {
                    letter = result[1];
                    number = result[2];
                    control = result[3];

                    cif = number.split('');

                    letters = ['J', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

                    sum = parseInt(cif[1], 10) + parseInt(cif[3], 10) + parseInt(cif[5], 10);
                    for (let index = 0; index < 7; index += 2) {
                        tmp = (2 * parseInt(cif[index], 10)).toString();
                        tmp = tmp.split('');
                        tmp = parseInt(tmp[0], 10) + ((tmp.length == 2) ? parseInt(tmp[1], 10) : 0);

                        sum += tmp;
                    }

                    let n = (10 - parseInt((sum.toString().substr(-1)), 10)) % 10;
                    if (['A', 'B', 'E', 'H'].includes(letter)) {
                        // Numerico
                        valid = (control == n);
                    } else if (['K', 'P', 'Q', 'S'].includes(letter)) {
                        // Letras
                        valid = (control == letters[n]);
                    } else {
                        // Alfanumérico
                        if (!isNaN(control)) {
                            valid = (control == n);
                        } else {
                            valid = (control == letters[n]);
                        }
                    }
                }
                break;
            case 3:
                pattern = /([X-Zx-z]{1})([0-9]{7})([A-Za-z])/;
                result = nif.match(pattern);
                if (result) {
                    letters = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
                    let nie_prefix;

                    switch (result[1]) {
                        case 'X':
                            nie_prefix = 0;
                            break;
                        case 'Y':
                            nie_prefix = 1;
                            break;
                        case 'Z':
                            nie_prefix = 2;
                            break;
                    }
                    let nie = parseInt(nie_prefix + '' + result[2]);
                    number = nie % 23;
                    if (result[3] == letter[number]) {
                        valid = true;
                    }
                }
                break;
            case 4:
                pattern = /^([A-Za-z0-9]{9})$/;
                result = nif.match(pattern);
                if (result) {
                    valid = true;
                }
                break;
            case 5:
                let oper: number[], val: number, digit: number;
                let lettersObj = {
                    'V': 4,
                    'E': 8,
                    'J': 12,
                    'P': 16,
                    'G': 20
                };
                oper = [3, 2, 7, 6, 5, 4, 3, 2];
                val = 0;
                result = nif.match(/^([VEJPG])-?([0-9]{8})-?([0-9])/i);
                if (result) {
                    val += lettersObj[result[1]];
                    number = result[2];
                    control = result[3];

                    for (let index = 0; index < oper.length; index++) {
                        val += oper[index] * number[index];
                    }
                    digit = 11 - (val % 11);
                    if (digit >= 10 || digit <= 0) {
                        digit = 0
                    }

                    valid = digit == parseInt(control)
                }
                break;
            case 6:
                result = nif.match(/^([0-9]{8,9})$/);
                if (result) {
                    valid = true;
                }
                break;
        }

        return valid;
    }

    getYearsAgo(nYears: number) {
        var years: number[], year: number;
        years = [];
        year = new Date().getFullYear();
        for (let index = 0; index < nYears; index++) {
            years.push(year);
            year++;
        }

        return years;
    }

    getTyinymceConfig() {
        return {
            'options': {
                plugins: [
                    'link lists autolink autoresize image',
                    'advlist charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table wordcount'
                ],
                toolbar_mode: 'wrap',
                toolbar: [
                    'undo redo | bold italic underline forecolor backcolor link image | alignleft aligncenter alignright alignfull alignjustify | numlist bullist outdent indent | fontselect fontsizeselect'
                ],
                language: this.translate.currentLang,
                file_picker_types: 'image',
                images_upload_handler: this.uploadImage,
                relative_urls: false,
                convert_urls: false,
                branding: false,
                contextmenu: false,
            },
            apiKey: TINYMCE_API_KEY
        };

    }

    uploadImage(blobInfo, success, failure, progress) {
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.open('POST', `${URL}${ENDPOINT}/tinymce/upload`);
        xhr.onload = function () {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status, { remove: true });
                return;
            }
            json = JSON.parse(xhr.responseText);
            if (json !== undefined) {
                if (json.status == 'OK') {
                    success(json.location);
                } else {
                    failure(json.msg, { remove: true });
                    return;
                }
            } else {
                failure('Invalid JSON: ' + xhr.responseText, { remove: true });
                return;
            }

        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        formData.append('uri', blobInfo.uri());
        xhr.send(formData);
    }



    base64ToImage(dataURI, mime) {
        const fileDate = dataURI.split(',');
        // const mime = fileDate[0].match(/:(.*?);/)[1];
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: mime });
        return blob;
    }

    /**
     * Convierte un objeto en un FormData
     * @param obj Objeto a convertir
     * @param exclude nombre de los elementos del objeto a excluir
     * @returns FormData
     */
    convertToFormData(obj, exclude: any[] = []): FormData {
        var form_data = new FormData();

        for (var key in obj) {
            if (!exclude.includes(key)) {
                if (typeof obj[key] === 'object') {
                    if (obj[key]) {
                        form_data.append(key, obj[key].id);
                    }
                } else {
                    form_data.append(key, obj[key]);
                }
            }
        }
        return form_data;
    }
}

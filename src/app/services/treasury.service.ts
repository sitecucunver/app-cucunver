import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from '../../environments/environment';
import { HelperService } from './helper.service';
import { ProfileQuotaYearsResponse, BalancesGraficReponse, BalancesResponse, BalanceResponse, Balance, Paymethod, PaymethodResponse, PaymethodsResponse, InvoicesResponse } from '../interfaces/treasury';
import { UiServiceService } from './ui-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Filter, GenericResponse } from '../interfaces/generic';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class TreasuryService {

    @Output() paidBalance = new EventEmitter<Balance>();
    @Output() paymethodNew = new EventEmitter<Paymethod>();
    @Output() paymethodEdit = new EventEmitter<Paymethod>();

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    async getBalanceGrafic(): Promise<BalancesGraficReponse> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.get<BalancesGraficReponse>(`${URL}${ENDPOINT}/balance/grafic`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async getProfileQuotaYears(included?: string, perPage?: number, filter?: Filter[], sort?: string, page?: number): Promise<ProfileQuotaYearsResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<ProfileQuotaYearsResponse>(`${URL}${ENDPOINT}/quota/profile/year/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async getBalances(included?: string, perPage?: number, filter?: Filter[], sort?: string, page?: number): Promise<BalancesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<BalancesResponse>(`${URL}${ENDPOINT}/balance/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async payBalance(balance: Balance): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<BalanceResponse>(`${URL}${ENDPOINT}/balance/pay`, balance, { headers }).subscribe({
                next: resp => {

                    if (resp.status == 'OK') {
                        this.paidBalance.emit(resp.result);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async getPaymethods(included?: string, perPage?: number, filter?: Filter[], sort?: string, page?: number): Promise<PaymethodsResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<PaymethodsResponse>(`${URL}${ENDPOINT}/paymethod/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async paymethodStore(paymethod: Paymethod): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<PaymethodResponse>(`${URL}${ENDPOINT}/paymethod/store`, paymethod, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.paymethodNew.emit(resp.result);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async paymethodSetDefault(paymethod: Paymethod): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/paymethod/default`, paymethod, { headers }).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error)
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async paymethodDelete(paymethod: Paymethod): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.delete<GenericResponse>(`${URL}${ENDPOINT}/paymethod/delete/${paymethod.id}`, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async paymethodUpdate(paymethod: Paymethod): Promise<boolean> {
        const headers = await this.helperService.getHeaders();
        return new Promise(resolve => {
            this.http.post<PaymethodResponse>(`${URL}${ENDPOINT}/paymethod/update`, paymethod, {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        this.paymethodEdit.emit(resp.result);
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    async getInvoice(included?: string, perPage?: number, filter?: Filter[], sort?: string, page?: number): Promise<InvoicesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<InvoicesResponse>(`${URL}${ENDPOINT}/invoice/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HelperService } from './helper.service';
import { NewsResponse, NewsByIdResponse } from '../interfaces/news';
import { Filter } from '../interfaces/generic';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class NewsService {

    constructor(
        private http: HttpClient,
        private helperService: HelperService
    ) { }

    async getNews(perPage?: number, included?: string, filter?: Filter[], sort?: string, page?: number): Promise<NewsResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<NewsResponse>(`${URL}${ENDPOINT}/news/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async getNewsById(id: number, included?: string): Promise<NewsByIdResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);
        return new Promise((resolve, reject) => {
            this.http.get<NewsByIdResponse>(`${URL}${ENDPOINT}/news/show/${id}${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { ActivitiesResponse, Activity, AttendanceReponse, ActivityResponse } from '../interfaces/activities';
import { Filter, GenericResponse } from '../interfaces/generic';
import { HelperService } from './helper.service';
import { UiServiceService } from './ui-service.service';

const URL = environment.url;
const ENDPOINT = environment.endpoint;

@Injectable({
    providedIn: 'root'
})
export class ActivitiesService {

    private acitivity: Activity;

    constructor(
        private http: HttpClient,
        private helperService: HelperService,
        private ui: UiServiceService,
        private translate: TranslateService
    ) { }

    async getActivities(perPage?: number, included?: string, filter?: Filter[], sort?: string, page?: number): Promise<ActivitiesResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<ActivitiesResponse>(`${URL}${ENDPOINT}/activity/index${parameters}`, { headers }).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async getActivityById(activity_id: number, included?: string): Promise<ActivityResponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included);
        return new Promise((resolve, reject) => {
            this.http.get<ActivityResponse>(`${URL}${ENDPOINT}/activity/show/${activity_id}${parameters}`, { headers }).subscribe({
                next: resp => {
                    resolve(resp);
                    this.setActivity(resp.result);
                },
                error: () => reject(),
            });
        });
    }

    async getAttendance(activity_id: number, perPage?: number, included?: string, filter?: Filter[], sort?: string, page?: number): Promise<AttendanceReponse> {
        const headers = await this.helperService.getHeaders();
        const parameters = this.helperService.createRouteParams(included, perPage, filter, sort, page);
        return new Promise((resolve, reject) => {
            this.http.get<AttendanceReponse>(`${URL}${ENDPOINT}/activity/attendance/index/${activity_id}${parameters}`, {headers}).subscribe({
                next: resp => resolve(resp),
                error: () => reject(),
            });
        });
    }

    async activityRegistered(activity: Activity) {
        const headers = await this.helperService.getHeaders();
        return new Promise((resolve) => {
            this.http.post<GenericResponse>(`${URL}${ENDPOINT}/activity/register`, activity,  {headers}).subscribe({
                next: resp => {
                    if (resp.status == 'OK') {
                        resolve(true);
                    } else {
                        this.ui.alertInfo(resp.error);
                        resolve(false);
                    }
                },
                error: () => {
                    this.ui.alertInfo(this.translate.instant('errors.genericerror'));
                    resolve(false);
                }
            });
        });
    }

    setActivity(activity: Activity) {
        this.acitivity = activity;
    }

    getActivity() {
        return this.acitivity;
    }
}

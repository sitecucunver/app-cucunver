import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'myentity',
        loadChildren: () => import('./pages/myentity/index/index.module').then(m => m.IndexPageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'myprofile',
        loadChildren: () => import('./pages/myprofile/index/index.module').then(m => m.IndexPageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'messages',
        loadChildren: () => import('./pages/messages/messages.module').then(m => m.MessagesPageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'news',
        loadChildren: () => import('./pages/news/news.module').then(m => m.NewsPageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'activities',
        loadChildren: () => import('./pages/activities/activities.module').then(m => m.ActivitiesPageModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'activity',
        loadChildren: () => import('./pages/activity/index/index.module').then(m => m.IndexPageModule),
        canLoad: [AuthGuard],
    },
    {
        path: 'configuration',
        loadChildren: () => import('./pages/configuration/configuration.module').then(m => m.ConfigurationPageModule),
        canLoad: [AuthGuard],
    },
    {
        path: 'deleted-account',
        loadChildren: () => import('./pages/deleted-account/deleted-account.module').then(m => m.DeletedAccountPageModule)
    },
    {
        path: 'under-contruction',
        loadChildren: () => import('./pages/under-contruction/under-contruction.module').then(m => m.UnderContructionPageModule)
    },
    {
        path: 'noentity',
        loadChildren: () => import('./pages/noentity/noentity.module').then(m => m.NoentityPageModule)
    },
    {
        path: 'entity-no-plan',
        loadChildren: () => import('./pages/entity-no-plan/entity-no-plan.module').then(m => m.EntityNoPlanPageModule)
    },
    {
        path: 'forgotten',
        loadChildren: () => import('./pages/forgotten/forgotten.module').then(m => m.ForgottenPageModule)
    },



];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

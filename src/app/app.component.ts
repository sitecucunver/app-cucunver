import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from './services/helper.service';
import { MenuOpt } from './interfaces/generic';
import { PushService } from './services/push.service';
import { BackButtonService } from './services/back-button.service';
import { ProfileService } from './services/profile.service';
import { IonMenu } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {

    @ViewChild(IonMenu) menu: IonMenu;

    menuOpts: Observable<MenuOpt[]>;

    constructor(
        private helperService: HelperService,
        private pushService: PushService,
        private backButtonService: BackButtonService,
        private profileService: ProfileService,
        private translate: TranslateService
    ) {
        this.menuOpts = this.helperService.getMenuOpts();
        this.pushService.init();
        this.backButtonService.init();

        this.profileService.changedProfile.subscribe(() => this.menu.close());
        const supportedLangs = ['es', 'gal', 'cat'];
        this.translate.addLangs(supportedLangs);
        this.translate.setDefaultLang('es');
        supportedLangs.forEach((language) => {
            this.translate.reloadLang(language);
        });
    }
}

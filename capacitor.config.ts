import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.cucunver.app',
  appName: 'cucunver',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
